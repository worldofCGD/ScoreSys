package cn.swust.score.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;


public class Candidate implements Serializable {
	public Long id;

	public String name;

	public String number;

	public String department;

	public String sex;

	public String education;

	public String post;

	public String positio;

	public String briefIntroduction;

	public Double totalScore;

	public String nation;

	public Date birthday;

	public String school;

	public String isInstructor;

	public String regularPayroll;

	public String academicDegree;

	public String politicalOutlook;

	public String officePhone;

	public String phone;

	public String email;

	public Integer sort;

	public Date createTime;


    private static final long serialVersionUID = 1L;


	public Long getId () {   
		return id;
	}
		
	public void setId (Long id) {
		this.id= id ;
	}
    public String getName () {   
    	 return name;
    }

    public void setName (String name) {
    	 this.name= name == null ? null : name.trim();
    }

    public String getNumber () {   
    	 return number;
    }

    public void setNumber (String number) {
    	 this.number= number == null ? null : number.trim();
    }

    public String getDepartment () {   
    	 return department;
    }

    public void setDepartment (String department) {
    	 this.department= department == null ? null : department.trim();
    }

    public String getSex () {   
    	 return sex;
    }

    public void setSex (String sex) {
    	 this.sex= sex == null ? null : sex.trim();
    }

    public String getEducation () {   
    	 return education;
    }

    public void setEducation (String education) {
    	 this.education= education == null ? null : education.trim();
    }

    public String getPost () {   
    	 return post;
    }

    public void setPost (String post) {
    	 this.post= post == null ? null : post.trim();
    }

    public String getPositio () {   
    	 return positio;
    }

    public void setPositio (String positio) {
    	 this.positio= positio == null ? null : positio.trim();
    }

    public String getBriefIntroduction () {   
    	 return briefIntroduction;
    }

    public void setBriefIntroduction (String briefIntroduction) {
    	 this.briefIntroduction= briefIntroduction == null ? null : briefIntroduction.trim();
    }


	public Double getTotalScore () {   
		  return totalScore;
	}
	
	public void setTotalScore (Double totalScore) {
		  this.totalScore= totalScore;
	}
    public String getNation () {   
    	 return nation;
    }

    public void setNation (String nation) {
    	 this.nation= nation == null ? null : nation.trim();
    }


	public Date getBirthday () {   
	    return birthday;
	}

	public void setBirthday (Date birthday) {
	    this.birthday= birthday;
	}
    public String getSchool () {   
    	 return school;
    }

    public void setSchool (String school) {
    	 this.school= school == null ? null : school.trim();
    }

    public String getIsInstructor () {   
    	 return isInstructor;
    }

    public void setIsInstructor (String isInstructor) {
    	 this.isInstructor= isInstructor == null ? null : isInstructor.trim();
    }

    public String getRegularPayroll () {   
    	 return regularPayroll;
    }

    public void setRegularPayroll (String regularPayroll) {
    	 this.regularPayroll= regularPayroll == null ? null : regularPayroll.trim();
    }

    public String getAcademicDegree () {   
    	 return academicDegree;
    }

    public void setAcademicDegree (String academicDegree) {
    	 this.academicDegree= academicDegree == null ? null : academicDegree.trim();
    }

    public String getPoliticalOutlook () {   
    	 return politicalOutlook;
    }

    public void setPoliticalOutlook (String politicalOutlook) {
    	 this.politicalOutlook= politicalOutlook == null ? null : politicalOutlook.trim();
    }

    public String getOfficePhone () {   
    	 return officePhone;
    }

    public void setOfficePhone (String officePhone) {
    	 this.officePhone= officePhone == null ? null : officePhone.trim();
    }

    public String getPhone () {   
    	 return phone;
    }

    public void setPhone (String phone) {
    	 this.phone= phone == null ? null : phone.trim();
    }

    public String getEmail () {   
    	 return email;
    }

    public void setEmail (String email) {
    	 this.email= email == null ? null : email.trim();
    }


	public Integer getSort () {   
		  return sort;
	}
	
	public void setSort (Integer sort) {
		  this.sort= sort ;
	}

	public Date getCreateTime () {   
	    return createTime;
	}

	public void setCreateTime (Date createTime) {
	    this.createTime= createTime;
	}
}