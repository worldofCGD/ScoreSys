package cn.swust.score.service;

import cn.swust.score.pojo.Candidate;
import java.util.List;
import com.github.pagehelper.PageInfo;

public interface CandidateService {

	int delById(Integer id);

    int save(Candidate record);

    Candidate getById(Integer id);

    int edit(Candidate record);
    
    PageInfo<Candidate> list(int pagenum, int pagesize); 
}