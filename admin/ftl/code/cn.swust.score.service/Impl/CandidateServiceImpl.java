package cn.swust.score.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.swust.score.dao.CandidateDao;
import cn.swust.score.pojo.Candidate;
import cn.swust.score.service.CandidateService;
import com.github.pagehelper.PageInfo;

@Service
public class CandidateServiceImpl implements CandidateService{

	@Autowired
	public CandidateDao candidatedao;
	
	@Override
	public int delById(Integer id) {
		return candidatedao.delById(id);
	}

	@Override
	public int save(Candidate record) {
		return candidatedao.save(record);
	}

	@Override
	public Candidate getById(Integer id) {
		return candidatedao.getById(id);
	}


	@Override
	public int edit(Candidate record) {
		return candidatedao.editById(record);
	}

	@Override
	public PageInfo<Candidate> list(int pagenum, int pagesize) {
		PageHelper.startPage(pagenum, pagesize);
		List<Candidate> datalist = candidatedao.list();
			 
		PageInfo<Candidate> p = new PageInfo<Candidate>(datalist);
		return p;
	}

}
