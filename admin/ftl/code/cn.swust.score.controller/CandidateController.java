package cn.swust.score.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.swust.score.pojo.Candidate;
import cn.swust.score.service.CandidateService;

@RestController
public class CandidateController {

	@Autowired
	public  candidateService;
	
	@ApiOperation(value="删除记录")
	@DeleteMapping(value="/del")
	public ResultResponse<Integer> delById(
	@ApiParam(required=true,value="查询记录编号") @RequestBody Integer id) {
		return new ResultResponse<Integer>( candidateService.delById(id));
	}
	

	@ApiOperation(value="新增记录")
	@PostMapping(value="/add")
	@ApiParam(required = true, value = "记录值")
	public ResultResponse<Candidate> save(@Valid  @RequestBody Candidate record) {
	    if(candidateService.save(record)<=0)
	        record=null;
	    else
	        record=candidateService.getById(record.getId());
		return new ResultResponse<Candidate>(record);
	}
	

	@ApiOperation(value = "根据ID查询记录") 
	@PutMapping(value = "/get")
	public ResultResponse<Candidate> getById(
			@ApiParam(required = true, value = "查询记录编号")
			@RequestBody @Valid Integer id) {
		return new ResultResponse<Candidate>(candidate.getById(id));
	}

	@ApiParam(required = true , value = "记录内容")
	@PutMapping(value = "/edit") 
	public ResultResponse<Candidate> editById(@Valid @RequestBody Candidate record) {
		if(candidateService.edit(record)<=0)
	        record=null;
	    else
	        record=candidateService.getById(record.getId());
		return new ResultResponse<Candidate>(record);
	}

	@ApiOperation(value = "详细列表查询") 
	@PostMapping("/list")
	public ResultResponse<PageInfo<Candidate>> list
			(@ApiParam(required = false, value = "查询参数")
			@Valid @RequestBody QueryCandidate param) {
		 return new  ResultResponse<PageInfo<Candidate>>(candidateService.list(param.getPageNum(),param.getPageSize()));
	}

}
