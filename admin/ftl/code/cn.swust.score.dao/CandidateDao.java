package cn.swust.score.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import cn.swust.score.pojo.Candidate;

@Mapper
public interface CandidateDao {
    int delById(Integer id);

    int save(Candidate record);

    Candidate getById(Integer id);

    int editById(Candidate record);
    
    List<Candidate> list(); 
}