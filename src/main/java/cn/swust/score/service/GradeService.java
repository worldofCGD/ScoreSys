package cn.swust.score.service;

import cn.swust.score.pojo.StageGrade;
import org.springframework.stereotype.Service;

import java.util.List;


public interface GradeService {

    List<StageGrade> commitService(int raterId,int stageId);


}
