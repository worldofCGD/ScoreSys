package cn.swust.score.service;

import cn.swust.score.pojo.QueryStage;
import cn.swust.score.pojo.Stage;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface StageService {

	int delById(Integer id);

    int save(Stage record);

    Stage getById(Integer id);

    int edit(Stage record);
    
    List<Stage> list(Integer scoreInfoId);
}