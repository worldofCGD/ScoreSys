package cn.swust.score.service;

import cn.swust.score.pojo.QueryRater;
import cn.swust.score.pojo.Rater;

import java.util.List;

import com.github.pagehelper.PageInfo;

public interface RaterService {

	int delById(Integer id);

    int save(Rater record);

	int save(List<Rater> record);
	
    Rater getById(Integer id);

    int edit(Rater record);
    
    PageInfo<Rater> list(int pagenum, int pagesize,QueryRater param); 
}