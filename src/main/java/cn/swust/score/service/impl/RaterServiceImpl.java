package cn.swust.score.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.swust.score.dao.RaterDao;
import cn.swust.score.pojo.Candidate;
import cn.swust.score.pojo.QueryRater;
import cn.swust.score.pojo.Rater;
import cn.swust.score.service.RaterService;
@Service
public class RaterServiceImpl implements RaterService {

	@Autowired
	RaterDao raterdao;
	@Override
	public int delById(Integer id) {
		// TODO Auto-generated method stub
		return raterdao.delById(id);
	}

	@Override
	public int save(Rater record) {
		// TODO Auto-generated method stub
		return raterdao.save(record);
	}

	@Override
	public Rater getById(Integer id) {
		// TODO Auto-generated method stub
		return raterdao.getById(id);
	}

	@Transactional
	@Override
	public int edit(Rater record) {
		// TODO Auto-generated method stub
		return raterdao.editById(record);
	}

	@Override
	public PageInfo<Rater> list(int pagenum, int pagesize,QueryRater param) {
		// TODO Auto-generated method stub
		if (pagenum !=0)
		{
			PageHelper.startPage(pagenum, pagesize);
			List<Rater> datalist = raterdao.list(param);
			 
			PageInfo<Rater> p = new PageInfo<Rater>(datalist);
			return p;
		}else
		{
			PageInfo<Rater>  p = new PageInfo<Rater>();
			List<Rater> datalist = raterdao.list(param);
			p.setList(datalist);
			return p;
		} 
	}

	@Transactional
	@Override
	public int save(List<Rater> record) {
		// TODO Auto-generated method stub
		for (int i = 0; i < record.size(); i++)
		{
			raterdao.save(record.get(i));
		} 
		return 0;
	}
	
	
}
