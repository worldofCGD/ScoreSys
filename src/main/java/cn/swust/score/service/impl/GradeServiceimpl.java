package cn.swust.score.service.impl;

import cn.swust.score.dao.CandidateDao;
import cn.swust.score.dao.CandidateStagInfoDao;
import cn.swust.score.dao.GradeDao;
import cn.swust.score.pojo.Grade;
import cn.swust.score.pojo.StageGrade;
import cn.swust.score.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class GradeServiceimpl implements GradeService {


    @Autowired
    GradeDao gradeDao;
    @Autowired
    CandidateStagInfoDao candidateStagInfoDao;

    /*
    vertify置为1，排名的计算与更新插入;
     */
    @Override
    public List<StageGrade> commitService(int raterId,int stageId){

        List<Grade> listAll= gradeDao.getAll(stageId);      //获取整张表;

        setVerify(listAll,raterId,stageId);       //将数据库verify信息写为不可修改:

        List<Grade> re=new ArrayList<Grade>();
        for(Grade g:listAll) {
            if(g.getRaterId()==raterId)g.setVerify(1);      //将本地listAll的verify设置为1;
            if (g.getVerify() == 0) re.add(g);      //删除verify==0的对象;

        }
        listAll.removeAll(re);

        List<StageGrade> candidateStagInfos=new ArrayList<StageGrade>();
        List<Integer> end=new ArrayList<Integer>();
        for(int i=0;i<listAll.size()-1;i++)
            if(listAll.get(i).getCandidateId()!=listAll.get(i+1).getCandidateId())
                end.add(i);
        end.add(listAll.size()-1);

        int p=0,q=0;
        double lowest;
        double highest;
        double average,count=0;
        for(int i=0;i<end.size();i++){
            q=end.get(i);
            lowest=highest=listAll.get(p).getFraction();
            for(int j=p;j<=q;j++){
                count+=listAll.get(j).getFraction();
                if(listAll.get(j).getFraction()<lowest)lowest=listAll.get(j).getFraction();
                if(listAll.get(j).getFraction()>highest)highest=listAll.get(j).getFraction();
            }
            if(q-p+1>2){
                average=(count-lowest-highest)/(q-p-1);
            }
            else{
                average=(count)/(q-p+1);
            }
            candidateStagInfos.add(new StageGrade(listAll.get(p).getCandidateId(),stageId,
                    BigDecimal.valueOf(highest),BigDecimal.valueOf(lowest),
                    BigDecimal.valueOf(average),0));
            p=q+1;
            count=0;
        }

        //获取candidate_stag_info中的此阶段已有信息，并合并到当前list中以计算排名
        List<StageGrade> oldList= getByStageId(stageId);

        //合并
        boolean isHave=false;
        int intermediatesCandidateId;
        for(int i=0;i<oldList.size();i++){
            intermediatesCandidateId=oldList.get(i).getCandidateId();
            for(int j=0;j<candidateStagInfos.size();j++){
                if(intermediatesCandidateId==candidateStagInfos.get(j).getCandidateId()){
                    isHave=true;
                    break;
                }
            }
            if(!isHave)candidateStagInfos.add(oldList.get(i));
            isHave=false;
        }

        //排名;
        StageGrade temp;
        for(int i=0;i<candidateStagInfos.size()-1;i++)
            for(int j=0;j<candidateStagInfos.size()-i-1;j++)
            {
                if(candidateStagInfos.get(j).getAverage().compareTo(candidateStagInfos.get(j+1).getAverage())<0){
                    temp=candidateStagInfos.get(j);
                    candidateStagInfos.set(j,candidateStagInfos.get(j+1));
                    candidateStagInfos.set(j+1,temp);
                }
            }
        for(int i=0;i<candidateStagInfos.size();i++)
            candidateStagInfos.get(i).setStageRanking(i+1);
        //数据库操作
      candidateStagInfos=updateAndSave(candidateStagInfos, oldList);

        //输出测试:
        for(StageGrade c:candidateStagInfos)
            System.out.println(c);
        return candidateStagInfos;
    }


    @Transactional
    public void setVerify(List<Grade> listAll,int raterId,int stageId){
        for(Grade g:listAll)
            if(g.getRaterId()==raterId)
                //将本地listAll的verify设置为1;
                gradeDao.updataSetVerify(raterId,stageId,g.getCandidateId());
    }

    @Transactional
    public List<StageGrade> getByStageId(int stageId){
        return candidateStagInfoDao.getByStageId(stageId);
    }

    @Transactional
    public List<StageGrade> updateAndSave(List<StageGrade> candidateStagInfos,
                                          List<StageGrade> oldList){
        boolean isUpdate=false;
        for(int i=0;i<candidateStagInfos.size();i++){
            for(int j=0;j<oldList.size();j++){
                if(oldList.get(j).getCandidateId()==candidateStagInfos.get(i).getCandidateId()){
                    StageGrade c=candidateStagInfos.get(i);
                    candidateStagInfoDao.editById(c);
                    isUpdate=true;
                }
            }
            if(!isUpdate) candidateStagInfoDao.save(candidateStagInfos.get(i));
            isUpdate=false;
        }
        return candidateStagInfos;
    }
}
