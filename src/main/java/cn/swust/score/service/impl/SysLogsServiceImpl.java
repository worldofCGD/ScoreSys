package cn.swust.score.service.impl;

import cn.swust.score.dao.SysLogsDao;
import cn.swust.score.pojo.QuerySysLogs;
import cn.swust.score.pojo.SysLogs;
import cn.swust.score.service.SysLogsService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
@Primary
@Service 
public class SysLogsServiceImpl  implements SysLogsService { 
	@Autowired
	private SysLogsDao sysLogsDao;
	
  
 
	@Override
	public int save(Long userId, String module, Boolean flag, String remark) {
		SysLogs sysLogs = new SysLogs();
		sysLogs.setFlag(flag);
		sysLogs.setModule(module);
		sysLogs.setRemark(remark);
//
//		SysUser user = UserUtil.getLoginUser(); 
//		sysLogs.setUserId(user.getId());

		return  sysLogsDao.save(sysLogs); 
	}

	@Override
	public int delMonthLogs(int year, int start, int end) {  
		int n = sysLogsDao.deleteLogs(year, start, end);
		//log.info("删除{}之前日志{}条", new Date(), n); 
		return n;
	}

	@Override
	public int delById(Integer id) {
		int n = sysLogsDao.delById(id); 
		return n;
	}

	@Override
	public SysLogs getById(Integer id) {
		SysLogs log = sysLogsDao.getById(id); 
		return log;
	}

	@Override
	public PageInfo<SysLogs> list(int pagenum, int pagesize, QuerySysLogs param) {
		PageHelper.startPage(pagenum, pagesize);
		List<SysLogs> datalist = sysLogsDao.list(param); 
		PageInfo<SysLogs> p = new PageInfo<SysLogs>(datalist); 
		return p;
	}

	@Override
	public int save(SysLogs logs) {
		return  sysLogsDao.save(logs); 
	} 
}
