package cn.swust.score.service.impl;

import cn.swust.score.dao.CandidateStagInfoDao;
import cn.swust.score.pojo.StageGrade;
import cn.swust.score.service.CandidateStagInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CandidateStagInfoServiceImpl implements CandidateStagInfoService {

	@Autowired
    CandidateStagInfoDao candidateStagInfoDao;

	@Override
	public int delById(Integer id) {
		return candidateStagInfoDao.delById(id);
	}

	@Override
	public int save(StageGrade record) {
		return candidateStagInfoDao.save(record);
	}

	@Override
	public StageGrade getById(Integer id) {
		return candidateStagInfoDao.getById(id);
	}

	@Override
	public int editById(StageGrade record) {
		return candidateStagInfoDao.editById(record);
	}

	@Override
	public List<StageGrade> getByStageId(int stageId) {
		return candidateStagInfoDao.getByStageId(stageId);
	}

	@Override
	public List<StageGrade> list() {
		return candidateStagInfoDao.list();
	}
}
