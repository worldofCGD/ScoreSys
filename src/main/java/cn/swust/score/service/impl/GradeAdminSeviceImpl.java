package cn.swust.score.service.impl;




import java.util.ArrayList;
import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.swust.score.dao.CandidateDao;
import cn.swust.score.dao.CandidateStagInfoDao;
import cn.swust.score.dao.GradeDao;
import cn.swust.score.dao.RaterDao;
import cn.swust.score.dao.ScoreInfoDao;
import cn.swust.score.dao.StageDao;
import cn.swust.score.pojo.Candidate;
import cn.swust.score.pojo.CandidateGrade;
import cn.swust.score.pojo.Grade;
import cn.swust.score.pojo.QueryCandidate;
import cn.swust.score.pojo.Rater;
import cn.swust.score.pojo.ShowGrade;
import cn.swust.score.pojo.StageGrade;
import cn.swust.score.service.GradeAdminService;
import org.springframework.beans.BeanUtils;
@Service
public class GradeAdminSeviceImpl implements GradeAdminService {
	@Autowired
	CandidateDao candidatedao;
	@Autowired
	RaterDao raterDao;

	@Autowired
	StageDao stageDao;

	@Autowired
	CandidateStagInfoDao staginfodao;

	@Autowired
	ScoreInfoDao scoreinfodao;

	@Autowired
	GradeDao gradeDao;
	 //数据库不完善导致出现NULL对象
	public List<ShowGrade> getCandidate(Integer stageId) {
		Integer scoreInfoId = stageDao.getScoreIdById(stageId);
		QueryCandidate qCandidate = new QueryCandidate();
		qCandidate.setScoreInfoId(scoreInfoId);
		List<Candidate> clist = candidatedao.list(qCandidate);
		List<Rater> rList = raterDao.getByScoreId(scoreInfoId);
		List<ShowGrade> resultList = new ArrayList<ShowGrade>();
		for (int i = 0; i < clist.size(); i++) {
			Candidate candidate = clist.get(i);
			ShowGrade sgGrade = new ShowGrade();
			BeanUtils.copyProperties(candidate, sgGrade);
			sgGrade.setStageId(stageId);
			List<Grade> glist = gradeDao.listByCandidateId(candidate.getId(), stageId);
			List<Grade> myGrades = new ArrayList<Grade>();
			sgGrade.setRaterGraded(myGrades);
			// 初始化，默认所有人未打分
			for (int j = 0; j < rList.size(); j++) {
				Grade grade = new Grade();
				grade.setRaterId(rList.get(j).getId());
				// 赋值
				myGrades.add(grade);
			}
			// 设置打分
			for (int j = 0; j < glist.size(); j++) {
				Grade realG = glist.get(j);
				for (int k = 0; k < myGrades.size(); k++) {
					Grade initG = myGrades.get(k);
					BeanUtils.copyProperties(initG, realG);
					if (realG.getRaterId() == initG.getRaterId()) {
						if (realG.getVerify() == 1)
							initG.setVerify(2);
						else {
							initG.setVerify(1);
						}
					}
				}
			}
		}
		return resultList;
	}
	 //数据库不完善导致出现NULL对象
	@Override
	public List<StageGrade> getStageGrade(Integer stageId) {
		Integer scoreInfoId = stageDao.getScoreIdById(stageId);
		QueryCandidate qCandidate = new QueryCandidate();
		qCandidate.setScoreInfoId(scoreInfoId);
		List<Candidate> clist = candidatedao.list(qCandidate);
		List<StageGrade> resultList = new ArrayList<StageGrade>();
		for (int i = 0; i < clist.size(); i++) {
			Candidate candidate = clist.get(i);
			StageGrade sGrade = new StageGrade();
			BeanUtils.copyProperties(candidate, sGrade);
			StageGrade sg = staginfodao.getGradeByCandidateId(candidate.getId(), stageId);
			BeanUtils.copyProperties(sg, sGrade);
			resultList.add(sGrade);
		}
		return resultList;
	}
 //数据库不完善导致出现NULL对象
	@Override
	public List<CandidateGrade> getAllGrade(Integer scoreInfoId) {
		QueryCandidate queryCandidate = new QueryCandidate();
		queryCandidate.setScoreInfoId(scoreInfoId);
		List<Candidate> cList = candidatedao.list(queryCandidate);
		List<CandidateGrade> result = new ArrayList<CandidateGrade>();
		for (int i = 0; i < cList.size(); i++) {
			Candidate candidate = cList.get(i);
			CandidateGrade cgrade = new CandidateGrade();
			BeanUtils.copyProperties(candidate, cgrade);
			cgrade.setStageScore1(staginfodao.getGradeByCandidateId(candidate.getId(), 1).getAverage());//一阶段分数
			cgrade.setStageScore2(staginfodao.getGradeByCandidateId(candidate.getId(), 2).getAverage());//二阶段分数
			result.add(cgrade);
		}
		return result;
	}
}
