package cn.swust.score.service.impl;

import java.util.List;

import cn.swust.score.dao.RaterDao;
import cn.swust.score.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.swust.score.dao.CandidateDao;
import cn.swust.score.dao.GradeDao;
import cn.swust.score.service.CandidateService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class CandidateServiceImpl implements CandidateService {

    @Autowired
    CandidateDao candidatedao;
    @Autowired
    GradeDao gradeDao;
    @Autowired
    RaterDao raterDao;

    @Override
    public int delById(Integer id) {
        return candidatedao.delById(id);
    }

    @Override
    public int save(Candidate record) {
        int i = candidatedao.save(record);
        // sort(record.getScoreInfoId());
        return i;
    }

    @Override
    public Candidate getById(Integer id) {
        return candidatedao.getById(id);
    }

    @Transactional
    @Override
    public int editById(Candidate record) {
        int i = candidatedao.editById(record);
        // sort(record.getScoreInfoId());
        return i;
    }

    @Override
    public PageInfo<Candidate> list(int pagenum, int pagesize, QueryCandidate param) {
        if (pagenum != 0) {
            PageHelper.startPage(pagenum, pagesize);
            List<Candidate> datalist = candidatedao.list(param);

            PageInfo<Candidate> p = new PageInfo<Candidate>(datalist);
            return p;
        } else {
            PageInfo<Candidate> p = new PageInfo<Candidate>();
            List<Candidate> datalist = candidatedao.list(param);
            p.setList(datalist);
            return p;
        }
    }

    public void sort(int scoreInfoId) {
        QueryCandidate qc = new QueryCandidate();
        qc.setScoreInfoId(scoreInfoId);
        List<Candidate> datalist = candidatedao.list(qc);

        for (int i = 0; i < datalist.size(); i++) {
            datalist.get(i).setSort(i + 1);
            this.editById(datalist.get(i));
        }

    }

    @Transactional
    @Override
    public int save(List<Candidate> record) {
        for (int i = 0; i < record.size(); i++) {
            candidatedao.save(record.get(i));
        }
        return 0;
    }

    @Override
    public Candidate getNext(Integer scoreInfoId, Integer sort) {
        return candidatedao.getNext(scoreInfoId, sort);
    }

    @Override
    public int setScore( int candidateId, int raterId, int stageId, int score) {
        // 如果verify =1 不能设置
        // 修改 如果已经打分，修改分数，否则新增分数
        Grade grade = gradeDao.get(raterId, stageId, candidateId);

        //获取评分人和候选人信息;
        Rater rater = raterDao.getById(grade.getRaterId());
        Candidate candidate = candidatedao.getById(grade.getCandidateId());

        if (candidate.getSchool().equals(rater.getSchool()))
            return 6;//"评分人和候选人学校相同，拒绝评分";

        else if (grade == null) {
            if (gradeDao.setScore(stageId,0, raterId, score) > 0) {
                return  1;//"新增成功";
            } else {
                return 2;//"新增失败";
            }
        } else if (grade.getVerify() == 0) {
            if (gradeDao.updateScore(stageId, candidateId, raterId, score) > 0) {
                return 3;//"修改成功";
            } else {
                return 4;//"修改失败";
            }
        } else {
            return 5;//"成绩已经确认不能修改";
        }
    }
    @Override
    public List<Grade> listByRaterId(int raterId, int stageId) {
        return gradeDao.listByRaterId(raterId, stageId);

    }

    @Override
    public List<Grade> listByCandidateId(int candidateId, int stageId) {
        return gradeDao.listByCandidateId(candidateId,stageId);
    }

    @Override
    public List<Grade> getAll(int stageId) {
        return gradeDao.getAll(stageId);
    }


    @Override
    public int updataSetVerify(int raterId, int stageId, int candidateId) {
        return gradeDao.updataSetVerify(raterId, stageId, candidateId);
    }


    @Override
    public void setRanking() {

    }
}



