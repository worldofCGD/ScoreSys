package cn.swust.score.service.impl;

import java.util.List;

import cn.swust.score.pojo.QueryScoreInfo;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.swust.score.dao.ScoreInfoDao;
import cn.swust.score.pojo.ScoreInfo;
import cn.swust.score.service.ScoreInfoService;
import com.github.pagehelper.PageInfo;

@Service
public class ScoreInfoServiceImpl implements ScoreInfoService{

	@Autowired
	public ScoreInfoDao scoreInfodao;
	
	@Override
	public int delById(Integer id) {
		return scoreInfodao.delById(id);
	}

	@Override
	public int save(ScoreInfo record) {
		return scoreInfodao.save(record);
	}

	@Override
	public ScoreInfo getById(Integer id) {
		return scoreInfodao.getById(id);
	}


	@Override
	public int edit(ScoreInfo record) {
		return scoreInfodao.editById(record);
	}

	@Override
	public List<ScoreInfo> list(QueryScoreInfo param) {
		return scoreInfodao.list(param);
	}

}
