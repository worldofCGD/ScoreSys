package cn.swust.score.service.impl;

import java.util.List;

import cn.swust.score.pojo.QueryStage;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.swust.score.dao.StageDao;
import cn.swust.score.pojo.Stage;
import cn.swust.score.service.StageService;
import com.github.pagehelper.PageInfo;

@Service
public class StageServiceImpl implements StageService{

	@Autowired
	public StageDao stagedao;
	
	@Override
	public int delById(Integer id) {
		return stagedao.delById(id);
	}

	@Override
	public int save(Stage record) {
		return stagedao.save(record);
	}

	@Override
	public Stage getById(Integer id) {
		return stagedao.getById(id);
	}

	@Override
	public int edit(Stage record) {
		return stagedao.editById(record);
	}

	@Override
	public List<Stage> list(Integer scoreInfoId) {
		 return stagedao.list(scoreInfoId);
	}

}
