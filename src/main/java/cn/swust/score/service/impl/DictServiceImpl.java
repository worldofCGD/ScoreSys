package cn.swust.score.service.impl;
 
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.swust.score.dao.DictDao;
import cn.swust.score.pojo.Dict;
import cn.swust.score.service.DictService; 

@Service
public class DictServiceImpl implements DictService {
	@Autowired
	protected  DictDao dictdao;
	
	 @Override
	public int delById(Integer id) { 
		return dictdao.delete(id);
	}
	 @Override
	public int save(Dict dict) { 
		return dictdao.save(dict);
	}
 
	 @Override
	public Dict getById(Integer id) {
		return dictdao.getById(id);
	}
 
	 @Override
	public int edit(Dict dict) {  
		return dictdao.update(dict);
			 
	} 
	 @Override
	public PageInfo<Dict> list(int pagenum, int pagesize) {
		PageHelper.startPage(pagenum, pagesize);
		List<Dict> datalist = dictdao.list(); 
		PageInfo<Dict> p = new PageInfo<Dict>(datalist); 
		return p;
	} 
}
