package cn.swust.score.service;
 
 
import com.github.pagehelper.PageInfo; 
import cn.swust.score.pojo.Dict; 

 
public interface DictService  { 
	 
	 int delById(Integer id);
 
	 int save(Dict dict) ; 
 
	 Dict getById(Integer id); 
 
	 int edit(Dict dict) ;
	 
	 PageInfo<Dict> list(int pagenum, int pagesize);
}
