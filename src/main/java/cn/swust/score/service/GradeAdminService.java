package cn.swust.score.service;



import java.util.List;

import cn.swust.score.pojo.CandidateGrade;
import cn.swust.score.pojo.ShowGrade;
import cn.swust.score.pojo.StageGrade;




public interface GradeAdminService {
	List<ShowGrade> getCandidate(Integer stageId);
	List<StageGrade> getStageGrade(Integer stageId);
	List<CandidateGrade> getAllGrade(Integer scoreInfoId);

}
