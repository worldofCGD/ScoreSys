package cn.swust.score.service;

import cn.swust.score.pojo.QueryScoreInfo;
import cn.swust.score.pojo.ScoreInfo;
import java.util.List;
import com.github.pagehelper.PageInfo;

public interface ScoreInfoService {

	int delById(Integer id);

    int save(ScoreInfo record);

    ScoreInfo getById(Integer id);

    int edit(ScoreInfo record);
    
    List<ScoreInfo> list(QueryScoreInfo param);
}