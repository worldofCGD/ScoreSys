package cn.swust.score.service;

import cn.swust.score.pojo.Candidate;
import cn.swust.score.pojo.Grade;
import cn.swust.score.pojo.QueryCandidate;

import java.util.List;


import com.github.pagehelper.PageInfo;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

public interface CandidateService {

	int delById(Integer id);
	
	int save(List<Candidate> record);

    int save(Candidate record);

    Candidate getById(Integer id);

    int editById(Candidate record);
    
    void sort(int scoreInfoId);
    
    Candidate getNext(Integer scoreInfoId, Integer sort);

    int setScore(int candidateId, int raterId,int stageId, int score);
    public PageInfo<Candidate> list(int pagenum, int pagesize, QueryCandidate param);

    List<Grade> listByRaterId(int raterId, int stageId );
    List<Grade> listByCandidateId(int candidateId,int stageId);

    List<Grade> getAll(int stageId);
    int updataSetVerify(int raterId,int stageId,int candidateId );

	void setRanking();

   
    

}