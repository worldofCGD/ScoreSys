package cn.swust.score.service;

import cn.swust.score.pojo.StageGrade;

import java.util.List;

public interface CandidateStagInfoService {

    int delById(Integer id);

    int save(StageGrade record);

    StageGrade getById(Integer id);

    int editById(StageGrade record);

    //获取此阶段candidate;
    List<StageGrade> getByStageId(int stageId);

    List<StageGrade> list();
}