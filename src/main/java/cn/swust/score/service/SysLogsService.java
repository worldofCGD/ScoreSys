package cn.swust.score.service;
 

import com.github.pagehelper.PageInfo;  
import cn.swust.score.pojo.QuerySysLogs;
import cn.swust.score.pojo.SysLogs;

public interface SysLogsService  
{    
	
	/**
	 * 删除记录
	 * 
	 * @param id
	 *            主键id
	 * @return 删除记录数量
	 */
	int delById(Integer id);
	 
 

	/**
	 * 根据主键查询记录
	 * 
	 * @param id
	 *            主键id
	 * @return 查询出的记录
	 */
	SysLogs getById(Integer id);
   
	  
    
    /**
     * 分页查询较详细记录 
     * @param page 查询条件及分页信息
     * @return 记录
     */
	PageInfo<SysLogs> list(int pagenum, int pagesize, QuerySysLogs param); 
    
      
    int save(SysLogs logs);
	int save(Long userId, String module, Boolean flag, String remark);
	 
	int delMonthLogs(int year, int start, int end);
}
