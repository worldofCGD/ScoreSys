package cn.swust.score;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

//扫描接口类，这个配置只能扫描该包下的接口，不能扫描mapper文件
@MapperScan(basePackages="cn.swust.score.dao")
@SpringBootApplication
public class SocreSysApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocreSysApplication.class, args);
	}
}
