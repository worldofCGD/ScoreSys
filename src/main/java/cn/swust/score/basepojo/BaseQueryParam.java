package cn.swust.score.basepojo;

import java.sql.Timestamp;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import io.swagger.annotations.ApiModelProperty;

public class BaseQueryParam {
	
	@ApiModelProperty(value = "第几页, 0为不分页") 
	@Min(0)
	protected Integer pageNum = 1;
	@ApiModelProperty(value = "分页大小") 
	@Min(1)
	@Max(40)
	protected Integer pageSize = 10;
	
	@ApiModelProperty(value = "建立用户id") 
	protected Long updateUserId; 
	
	@ApiModelProperty(value = "修改用户id") 
	protected Long createUserId;   

	@ApiModelProperty(value = "查询修改开始时间") 
	protected Timestamp updateTimeStart; 
	
	@ApiModelProperty(value = "查询建单开始时间") 
	protected Timestamp createTimeStart;
	
	@ApiModelProperty(value = "查询修改结束时间") 
	protected Timestamp updateTimeEnd; 
	
	@ApiModelProperty(value = "查询建单结束时间") 
	protected Timestamp createTimeEnd;

	public Timestamp getUpdateTimeStart() {
		return updateTimeStart;
	}

	public void setUpdateTimeStart(Timestamp updateTimeStart) {
		this.updateTimeStart = updateTimeStart;
	}

	public Timestamp getCreateTimeStart() {
		return createTimeStart;
	}

	public void setCreateTimeStart(Timestamp createTimeStart) {
		this.createTimeStart = createTimeStart;
	}

	public Timestamp getUpdateTimeEnd() {
		return updateTimeEnd;
	}

	public void setUpdateTimeEnd(Timestamp updateTimeEnd) {
		this.updateTimeEnd = updateTimeEnd;
	}

	public Timestamp getCreateTimeEnd() {
		return createTimeEnd;
	}

	public void setCreateTimeEnd(Timestamp createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}


	public Integer getPageNum() {
		return pageNum;
	}


	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}


	public Integer getPageSize() {
		return pageSize;
	}


	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}


	public Long getUpdateUserId() {
		return updateUserId;
	}


	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}


	public Long getCreateUserId() {
		return createUserId;
	}


	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	} 

}
