package cn.swust.score.advice;
 
 

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import cn.swust.score.basepojo.ResultResponse;
 
/**
 * springmvc异常处理
 */ 

@RestControllerAdvice
public class ExceptionHandlerAdvice { 

	private static final Logger log = LoggerFactory.getLogger("adminLogger"); 

	@ExceptionHandler({ IllegalArgumentException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResultResponse<Object> badRequestException(IllegalArgumentException exception) {
		return new ResultResponse<Object>(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
	} 

	@ExceptionHandler({ MissingServletRequestParameterException.class, HttpMessageNotReadableException.class,
			UnsatisfiedServletRequestParameterException.class, MethodArgumentTypeMismatchException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResultResponse<Object> badRequestException(Exception exception) {
		return new ResultResponse<Object>(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
	}

	@ExceptionHandler(Throwable.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResultResponse<Object> exception(Throwable throwable) { 
		log.error("系统异常", throwable);
		return new ResultResponse<Object>(HttpStatus.INTERNAL_SERVER_ERROR.value(), throwable.getMessage()); 
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResultResponse<Object> violationException(HttpServletRequest req, ConstraintViolationException throwable) {
		log.error("数据验证错误", throwable); 
		String errorMsg = "";
 
		for (ConstraintViolation<?> constraintViolation : throwable.getConstraintViolations()) { 
 		 errorMsg += constraintViolation.getPropertyPath().toString() +":"
					 + constraintViolation.getMessage() +";";
		 } 
	 	return new ResultResponse<Object>(222, errorMsg); 
	}
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResultResponse<Object> bindException(HttpServletRequest req, MethodArgumentNotValidException throwable) {
		log.error("数据验证错误", throwable); 
		String errorMsg = "";
		 for (FieldError error : throwable.getBindingResult().getFieldErrors()) {
	            // 获得验证失败的类 constraintViolation.getLeafBean()
	            // 获得验证失败的值 constraintViolation.getInvalidValue()
	            // 获取参数值 constraintViolation.getExecutableParameters()
	            // 获得返回值 constraintViolation.getExecutableReturnValue()
  		    errorMsg += error.getField() +":"
					 + error.getDefaultMessage() +";"; 
		 } 
	 	return new ResultResponse<Object>(222, errorMsg); 
	} 
	
}
