package cn.swust.score.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Email;

@JsonInclude(Include.NON_NULL)
public class Candidate implements Serializable {
	
	
	@Excel(name="候选人id",orderNum="1")
	@ApiModelProperty(value = "候选人id") 
	@NotNull
	@Min(1)
	public Integer id;
	
	@Excel(name="候选人姓名",orderNum="2")
	@ApiModelProperty(value = "候选人姓名") 
	@NotNull
	@Size(min=1,max=20)
	public String name;

	@Excel(name="候选人工号",orderNum="3")
	@ApiModelProperty(value = "候选人工号")  
	@Size(min=1,max=20)
	public String number;
	
	@Excel(name="候选人部门",orderNum="4")
	@ApiModelProperty(value = "候选人部门") 
 
	@Size(min=1,max=20)
	public String department;

	@Excel(name="候选人性别",orderNum="5")
	@ApiModelProperty(value = "候选人性别")
	@NotNull
	public String sex;

	@Excel(name="候选人学历",orderNum="6")
	@ApiModelProperty(value = "候选人学历") 
	@Size(min=1,max=50)
	public String education;

	@Excel(name="候选人职务",orderNum="7")
	@ApiModelProperty(value = "候选人职务") 
	@Size(min=1,max=50)
	public String post;

	@Excel(name="候选人职称",orderNum="8")
	@ApiModelProperty(value = "候选人职称") 
	@Size(min=1,max=50)
	public String position;

	@Excel(name="候选人简短介绍",orderNum="9")
	@ApiModelProperty(value = "候选人简短介绍") 
	public String briefIntroduction;
	
	@Excel(name="候选人总分",orderNum="10")
	@ApiModelProperty(value = "候选人总分")  
	public Double totalScore;

	@Excel(name="候选人名族",orderNum="11")
	@ApiModelProperty(value = "候选人名族") 
	@Size(min=1,max=10)
	public String nation;

	@ApiModelProperty(value = "候选人生日")
	@Excel(name="候选人生日", format="yyyy-MM-dd",orderNum="12")
	public Date birthday;

	@Excel(name="候选人学校", orderNum="13")
	@ApiModelProperty(value = "候选人学校") 
	@Size(min=1,max=255)
	public String school;

	@Excel(name="近两年是否担任专职辅导员",orderNum="14")
	@ApiModelProperty(value = "近两年是否担任专职辅导员") 
	@Size(min=1,max=10)
	public String isInstructor;

	@Excel(name="是否在岗在编",orderNum="15")
	@ApiModelProperty(value = "是否在岗在编") 
	@Size(min=1,max=10)
	public String regularPayroll;

	@Excel(name="候选人学位",orderNum="16")
	@ApiModelProperty(value = "候选人学位") 
	@Size(min=1,max=10)
	public String academicDegree;

	@Excel(name="候选人政治面貌",orderNum="17")
	@ApiModelProperty(value = "候选人政治面貌") 
	@Size(min=1,max=100)
	public String politicalOutlook;

	@Excel(name="候选人办公电话",orderNum="18")
	@ApiModelProperty(value = "候选人办公电话") 
	@Size(min=1,max=100)
	public String officePhone;

	@Excel(name="候选人电话",orderNum="19")
	@ApiModelProperty(value = "候选人电话") 
	@Size(min=1,max=100)
	public String phone;

	@Excel(name="候选人邮箱",orderNum="20")
	@ApiModelProperty(value = "候选人邮箱") 
	@Email
	public String email;

	@Excel(name="评分顺序",orderNum="21")
	@ApiModelProperty(value = "评分顺序") 
	@Min(1)
	public Integer sort;
	
	@Excel(name="评分ID",orderNum="23")
	@ApiModelProperty(value = "评分ID") 
	@Min(1)
	@NotNull
	public Integer scoreInfoId;
	
	@Excel(name="初始得分1",orderNum="24")
	@ApiModelProperty(value = "初始得分1") 
	public BigDecimal initialScore1;

	@Excel(name="初始得分2",orderNum="25")
	@ApiModelProperty(value = "初始得分2") 
	public BigDecimal initialScore2;
	
	@Excel(name="总排名",orderNum="26")
	@ApiModelProperty(value = "总排名") 
	public Integer Ranking;
	 
	
	public Integer getRanking() {
		return Ranking;
	}

	public void setRanking(Integer ranking) {
		Ranking = ranking;
	}

	public Integer getScoreInfoId() {
		return scoreInfoId;
	}

	public void setScoreInfoId(Integer scoreInfoId) {
		this.scoreInfoId = scoreInfoId;
	}

	@ApiModelProperty(value = "填表时间")
	@Excel(name="填表时间",databaseFormat="yyMMddHHmmss",format="yyyy-MM-dd",orderNum="23")
	private Timestamp createTime;


    private static final long serialVersionUID = 1L;


	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Integer getId () {   
		return id;
	}
		
	public void setId (Integer id) {
		this.id= id ;
	}
    public String getName () {   
    	 return name;
    }

    public void setName (String name) {
    	 this.name= name == null ? null : name.trim();
    }

    public String getNumber () {   
    	 return number;
    }

    public void setNumber (String number) {
    	 this.number= number == null ? null : number.trim();
    }

    public String getDepartment () {   
    	 return department;
    }

    public void setDepartment (String department) {
    	 this.department= department == null ? null : department.trim();
    }

    public String getSex () {   
    	 return sex;
    }

    public void setSex (String sex) {
    	 this.sex= sex == null ? null : sex.trim();
    }

    public String getEducation () {   
    	 return education;
    }

    public void setEducation (String education) {
    	 this.education= education == null ? null : education.trim();
    }

    public String getPost () {   
    	 return post;
    }

    public void setPost (String post) {
    	 this.post= post == null ? null : post.trim();
    }

    public String getPosition () {   
    	 return position;
    }

    public void setPosition (String position) {
    	 this.position= position == null ? null : position.trim();
    }

    public String getBriefIntroduction () {   
    	 return briefIntroduction;
    }

    public void setBriefIntroduction (String briefIntroduction) {
    	 this.briefIntroduction= briefIntroduction == null ? null : briefIntroduction.trim();
    }


	public Double getTotalScore () {   
		  return totalScore;
	}
	
	public void setTotalScore (Double totalScore) {
		  this.totalScore= totalScore;
	}
    public String getNation () {   
    	 return nation;
    }

    public void setNation (String nation) {
    	 this.nation= nation == null ? null : nation.trim();
    }


	public Date getBirthday () {   
	    return birthday;
	}

	public void setBirthday (Date birthday) {
	    this.birthday= birthday;
	}
    public String getSchool () {   
    	 return school;
    }

    public void setSchool (String school) {
    	 this.school= school == null ? null : school.trim();
    }

    public String getIsInstructor () {   
    	 return isInstructor;
    }

    public void setIsInstructor (String isInstructor) {
    	 this.isInstructor= isInstructor == null ? null : isInstructor.trim();
    }

    public String getRegularPayroll () {   
    	 return regularPayroll;
    }

    public void setRegularPayroll (String regularPayroll) {
    	 this.regularPayroll= regularPayroll == null ? null : regularPayroll.trim();
    }

    public String getAcademicDegree () {   
    	 return academicDegree;
    }

    public void setAcademicDegree (String academicDegree) {
    	 this.academicDegree= academicDegree == null ? null : academicDegree.trim();
    }

    public String getPoliticalOutlook () {   
    	 return politicalOutlook;
    }

    public void setPoliticalOutlook (String politicalOutlook) {
    	 this.politicalOutlook= politicalOutlook == null ? null : politicalOutlook.trim();
    }

    public String getOfficePhone () {   
    	 return officePhone;
    }

    public void setOfficePhone (String officePhone) {
    	 this.officePhone= officePhone == null ? null : officePhone.trim();
    }

    public String getPhone () {   
    	 return phone;
    }

    public void setPhone (String phone) {
    	 this.phone= phone == null ? null : phone.trim();
    }

    public String getEmail () {   
    	 return email;
    }

    public void setEmail (String email) {
    	 this.email= email == null ? null : email.trim();
    }


	public Integer getSort () {   
		  return sort;
	}
	
	public void setSort (Integer sort) {
		  this.sort= sort ;
	}

	
	public BigDecimal getInitialScore1() {
		return initialScore1;
	}

	public void setInitialScore1(BigDecimal initialScore1) {
		this.initialScore1 = initialScore1;
	}

	public BigDecimal getInitialScore2() {
		return initialScore2;
	}

	public void setInitialScore2(BigDecimal initialScore2) {
		this.initialScore2 = initialScore2;
	}
	
	@Override
	public String toString() {
		return "Candidate [id=" + id + ", name=" + name + ", number=" + number + ", department=" + department + ", sex="
				+ sex + ", education=" + education + ", post=" + post + ", position=" + position + ", briefIntroduction="
				+ briefIntroduction + ", totalScore=" + totalScore + ", nation=" + nation + ", birthday=" + birthday
				+ ", school=" + school + ", isInstructor=" + isInstructor + ", regularPayroll=" + regularPayroll
				+ ", academicDegree=" + academicDegree + ", politicalOutlook=" + politicalOutlook + ", officePhone="
				+ officePhone + ", phone=" + phone + ", email=" + email + ", sort=" + sort + "]";
	}


}