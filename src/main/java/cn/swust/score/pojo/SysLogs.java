package cn.swust.score.pojo;

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
@JsonInclude(Include.NON_NULL)
public class SysLogs  {
   
	private Integer id;
	@Size(min=1, max=50 )
	@ApiModelProperty(value = "模块")
	@Excel(name="模块",orderNum="1")
	private String module;
	
	@NotNull
	@ApiModelProperty(value = "成功/失败")
	@Excel(name="flag",orderNum="1")
	private Boolean flag;
	
	@ApiModelProperty(value = "异常备注")
	@Excel(name="remark",orderNum="1")
	private String remark;  
	
	@ApiModelProperty(value = "创建时间")
	@Excel(name="createTime",databaseFormat="yyMMddHHmmss",format="yyyy-MM-dd",orderNum="1")
	private Timestamp createTime; 
	
	@ApiModelProperty(value = "用户id")
	@Excel(name="userId",orderNum="1")
	private Long userId;
	
	@ApiModelProperty(value = "用户号")
	@Excel(name="username",orderNum="1")
	private String username;
	
	@ApiModelProperty(value = "用户别名")
	@Excel(name="nickname",orderNum="1")
	private String nickname;
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userid) {
		this.userId = userid;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
 
}
