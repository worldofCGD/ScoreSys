package cn.swust.score.pojo;

import java.math.BigDecimal;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

public class StageGrade extends Candidate{
	@ApiModelProperty(value = "阶段id")
	@Excel(name="阶段id",orderNum="1")
	private Integer stageId;
	
	@ApiModelProperty(value = "候选人id")
	@Excel(name="阶段id",orderNum="2")
	private Integer candidateId;
	
	public Integer getCandidateId() {
		return candidateId;
	}
	public void setCandidateId(Integer candidateId) {
		this.candidateId = candidateId;
	}
	@ApiModelProperty(value = "阶段名字")
	@Excel(name="阶段名字",orderNum="3")
	private char stageName;
	
	@ApiModelProperty(value = "该阶段最高分")
	@Excel(name="该阶段最高得分",orderNum="4")
	private BigDecimal highestScore;
	
	@ApiModelProperty(value = "该阶段最低分")
	@Excel(name="该阶段最低得分",orderNum="5")
	private BigDecimal lowestScore;
	
	@ApiModelProperty(value = "该阶段平均分")
	@Excel(name="该阶段平均分",orderNum="6")
	private BigDecimal average;
	
	@ApiModelProperty(value = "该阶段排名")
	@Excel(name="该阶段排名",orderNum="7")
	private Integer stageRanking;
	public StageGrade(){

	}

	public StageGrade(Integer candidateId, Integer stageId, BigDecimal highestScore, BigDecimal lowestScore, BigDecimal average, Integer stageRanking) {
		this.candidateId = candidateId;
		this.stageId=stageId;
		this.highestScore = highestScore;
		this.lowestScore = lowestScore;
		this.average = average;
		this.stageRanking = stageRanking;
	}
	public Integer getStageId() {
		return stageId;
	}
	public void setStageId(Integer stageId) {
		this.stageId = stageId;
	}
	public BigDecimal getHighestScore() {
		return highestScore;
	}
	public void setHighestScore(BigDecimal highestScore) {
		this.highestScore = highestScore;
	}
	public BigDecimal getLowestScore() {
		return lowestScore;
	}
	public void setLowestScore(BigDecimal lowestScore) {
		this.lowestScore = lowestScore;
	}
	public BigDecimal getAverage() {
		return average;
	}
	public void setAverage(BigDecimal average) {
		this.average = average;
	}
	public Integer getStageRanking() {
		return stageRanking;
	}
	public void setStageRanking(Integer stageRanking) {
		this.stageRanking = stageRanking;
	}
	public char getStageName() {
		return stageName;
	}
	public void setStageName(char stageName) {
		this.stageName = stageName;
	}

	@Override
	public String toString() {
		return "StageGrade{" +
				"stageId=" + stageId +
				", candidateId=" + candidateId +
				", stageName=" + stageName +
				", highestScore=" + highestScore +
				", lowestScore=" + lowestScore +
				", average=" + average +
				", stageRanking=" + stageRanking +
				'}';
	}
}
