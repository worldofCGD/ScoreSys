package cn.swust.score.pojo;
 

import java.sql.Timestamp;  
import javax.validation.constraints.Min;

import cn.swust.score.basepojo.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty; 
 

public class QuerySysLogs extends BaseQueryParam  {
 
	@ApiModelProperty(value = "年份") 
	@Min(2018)
	private Long year; 
	 
	@ApiModelProperty(value = "模块")
	private String module;
	 
	@ApiModelProperty(value = "成功失败")
	private Boolean flag; 
	
 
	@ApiModelProperty(value = "用户id") 
	private Long userId; 
	
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getYear() {
		return year;
	}

	public void setYear(Long year) {
		this.year = year;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	public Timestamp getCreateTimeStart() {
		return createTimeStart;
	}

	public void setCreateTimeStart(Timestamp createTimeStart) {
		this.createTimeStart = createTimeStart;
	}

	public Timestamp getCreateTimeEnd() {
		return createTimeEnd;
	}

	public void setCreateTimeEnd(Timestamp createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}  
}
