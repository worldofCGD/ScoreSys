package cn.swust.score.pojo;

import cn.swust.score.basepojo.BaseQueryParam;

import java.sql.Date;

public class QueryScoreInfo  {


    public Long id;

    public String scoringTitle;

    public Date scoringTime;

    private static final long serialVersionUID = 1L;

 

    public Long getId() {
        return id;
    }
    public Date getScoringTime() {
        return scoringTime;
    }
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getScoringTitle() {
        return scoringTitle;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setScoringTime(Date scoringTime) {
        this.scoringTime = scoringTime;
    }

    public void setScoringTitle(String scoringTitle) {
        this.scoringTitle = scoringTitle;
    }

}
