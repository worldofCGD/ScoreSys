package cn.swust.score.pojo;
 
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size; 
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import cn.swust.score.pojo.Grade;

public class ShowGrade {
	@Excel(name="候选人id",orderNum="1")
	@ApiModelProperty(value = "候选人id") 
	@NotNull
	@Min(1)
	public Integer id;
	
	@Excel(name="候选人姓名",orderNum="2")
	@ApiModelProperty(value = "候选人姓名") 
	@NotNull
	@Size(min=1,max=20)
	public String name;

	@Excel(name="候选人工号",orderNum="3")
	@ApiModelProperty(value = "候选人编号")  
	@Size(min=1,max=20)
	public String number;
	
	@ApiModelProperty(value = "各个评委是否打分") 
	List<Grade> raterGraded;
	
	@ApiModelProperty(value = "初始得分1") 
	Number initial_score1;
	@ApiModelProperty(value = "初始得分1") 
	Number initial_score2;
	@ApiModelProperty(value = "阶段Id") 
	Integer StageId;
	
	@ApiModelProperty(value = "之前阶段得分,如果是1阶段得分，该项为null") 
	Number []preScore; 
	
	@ApiModelProperty(value = "当前最高分") 
	Number maxScore = 0;
	@ApiModelProperty(value = "当前最低分") 
	Number minScore = 0;
	@ApiModelProperty(value = "当前平均分") 
	Number average = 0;
	
	@ApiModelProperty(value = "汇总得分") 
	Number sum;
	
	@ApiModelProperty(value = "汇总排名") 
	Integer rank;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public List<Grade> getRaterGraded() {
		return raterGraded;
	}

	public void setRaterGraded(List<Grade> raterGraded) {
		this.raterGraded = raterGraded;
	}

	public Number getInitial_score1() {
		return initial_score1;
	}

	public void setInitial_score1(Number initial_score1) {
		this.initial_score1 = initial_score1;
	}

	public Number getInitial_score2() {
		return initial_score2;
	}

	public void setInitial_score2(Number initial_score2) {
		this.initial_score2 = initial_score2;
	}

	public Integer getStageId() {
		return StageId;
	}

	public void setStageId(Integer stageId) {
		StageId = stageId;
	}

	public Number[] getPreScore() {
		return preScore;
	}

	public void setPreScore(Number[] preScore) {
		this.preScore = preScore;
	}

	public Number getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(Number maxScore) {
		this.maxScore = maxScore;
	}

	public Number getMinScore() {
		return minScore;
	}

	public void setMinScore(Number minScore) {
		this.minScore = minScore;
	}

	public Number getAverage() {
		return average;
	}

	public void setAverage(Number average) {
		this.average = average;
	}

	public Number getSum() {
		return sum;
	}

	public void setSum(Number sum) {
		this.sum = sum;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	
	
}
