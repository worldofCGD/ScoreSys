package cn.swust.score.pojo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

public class CandidateGrade extends Candidate{ 
	
	
	@ApiModelProperty(value = "理论宣讲得分") 
	@Excel(name="理论宣讲得分",orderNum="1")
	Number stageScore1;
	
	@ApiModelProperty(value = "谈心谈话得分") 
	@Excel(name="谈心谈话得分",orderNum="2")
	Number stageScore2;
	
	

	public Number getStageScore1() {
		return stageScore1;
	}

	public void setStageScore1(Number stageScore1) {
		this.stageScore1 = stageScore1;
	}

	public Number getStageScore2() {
		return stageScore2;
	}

	public void setStageScore2(Number stageScore2) {
		this.stageScore2 = stageScore2;
	}

}
