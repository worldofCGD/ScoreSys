package cn.swust.score.pojo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Date;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class Stage implements Serializable {

	@ApiModelProperty(value = "id")
	@Min(1)
	public Integer id;

	@Excel(name="姓名",orderNum="1")
	@ApiModelProperty(value = "姓名")
	@NotNull
	@Size(min = 1,max =20 )
	public String name;

	@Excel(name="开始时间",orderNum="2")
	@ApiModelProperty(value = "开始时间")
	@NotNull
	public Date startTime;

	@Excel(name="结束时间",orderNum="3")
	@ApiModelProperty(value = "结束时间")
	@NotNull
	public Date endTime;

	@Excel(name="最高分",orderNum="4")
	@ApiModelProperty(value = "最高分")
	@NotNull
	@Size(min = 1,max =3 )
	public Double highestScore;

	@Excel(name="最低分",orderNum="5")
	@ApiModelProperty(value = "最低分")
	@NotNull
	@Size(min = 1,max =3 )
	public Double lowestScore;

	@Excel(name="平均分",orderNum="6")
	@ApiModelProperty(value = "平均分")
	@NotNull
	@Size(min = 1,max =3 )
	public Double average;

	@ApiModelProperty(value = "分数信息id")
	public Integer scoreInfoId;

	@Excel(name="分制",orderNum="7")
	@ApiModelProperty(value = "分制")
	@NotNull
	@Size(min = 1,max =3 )
	public String fullMarks;


    private static final long serialVersionUID = 1L;


	public Integer getId () {
		return id;
	}
		
	public void setId (Integer id) {
		this.id= id ;
	}
    public String getName () {   
    	 return name;
    }

    public void setName (String name) {
    	 this.name= name == null ? null : name.trim();
    }


	public Date getStartTime () {
	    return startTime;
	}

	public void setStartTime (Date startTime) {
	    this.startTime= startTime;
	}

	public Date getEndTime () {
	    return endTime;
	}

	public void setEndTime (Date endTime) {
	    this.endTime= endTime;
	}

	public Double getHighestScore () {   
		  return highestScore;
	}
	
	public void setHighestScore (Double highestScore) {
		  this.highestScore= highestScore;
	}

	public Double getLowestScore () {   
		  return lowestScore;
	}
	
	public void setLowestScore (Double lowestScore) {
		  this.lowestScore= lowestScore;
	}

	public Double getAverage () {   
		  return average;
	}
	
	public void setAverage (Double average) {
		  this.average= average;
	}

	public Integer getScoreInfoId () {   
		  return scoreInfoId;
	}
	
	public void setScoreInfoId (Integer scoreInfoId) {
		  this.scoreInfoId= scoreInfoId ;
	}
    public String getFullMarks () {   
    	 return fullMarks;
    }

    public void setFullMarks (String fullMarks) {
    	 this.fullMarks= fullMarks == null ? null : fullMarks.trim();
    }

	@Override
	public String toString() {
		return "Stage{" +
				"id=" + id +
				", name='" + name + '\'' +
				", startTime=" + startTime +
				", endTime=" + endTime +
				", highestScore=" + highestScore +
				", lowestScore=" + lowestScore +
				", average=" + average +
				", scoreInfoId=" + scoreInfoId +
				", fullMarks='" + fullMarks + '\'' +
				'}';
	}
}