package cn.swust.score.pojo;

import java.io.Serializable;
import java.util.List; 
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
@JsonInclude(Include.NON_NULL)
public class Permission   {

	@ApiModelProperty(value = "id") 
	@Min(1)
	private Long id;
	
	@Excel(name="parentId",orderNum="0")
	@ApiModelProperty(value = "上级菜单id") 
	@Min(1)
	private Long parentId;
	
	@Excel(name="name",orderNum="1")
	@ApiModelProperty(value = "名称")
	@NotNull
	@Size(min=1, max=50)
	private String name;
	
	@Excel(name="icon",orderNum="2")
	@ApiModelProperty(value = "icon") 
	private String icon;
	
	@Excel(name="href",orderNum="3")
	@ApiModelProperty(value = "链接") 
	private String href;
	
	@Excel(name="route",orderNum="4")
	@ApiModelProperty(value = "链接") 
	private String route;
	
	@Excel(name="mpid",orderNum="5")
	@ApiModelProperty(value = "mpid") 
	private Long mpid;
	
	@Excel(name="bpid",orderNum="6")
	@ApiModelProperty(value = "bpid") 
	private Long bpid; 
	
	@Excel(name="type",orderNum="7")
	@ApiModelProperty(value = "类型") 
	private Integer type;
	
	@Excel(name="permission",orderNum="8")
	@ApiModelProperty(value = "权限") 
	private String permission;
	
	@Excel(name="sort",orderNum="9")
	@ApiModelProperty(value = "排序") 
	@Min(1)
	private Integer sort; 
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Excel(name="memo",orderNum="10")
	@ApiModelProperty(value = "备注") 
	private String memo;
	
	
	private List<Permission> child;

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	} 

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public List<Permission> getChild() {
		return child;
	}

	public void setChild(List<Permission> child) {
		this.child = child;
	}
	
	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public Long getMpid() {
		return mpid;
	}

	public void setMpid(Long mpid) {
		this.mpid = mpid;
	}

	public Long getBpid() {
		return bpid;
	}

	public void setBpid(Long bpid) {
		this.bpid = bpid;
	}
 
 
}
