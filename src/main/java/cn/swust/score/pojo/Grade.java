package cn.swust.score.pojo;

import java.sql.Timestamp; 
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

public class Grade {

	@ApiModelProperty(value = "候选人性别")
	String candidateSex;

	@ApiModelProperty(value = "候选人学历")
	String candidateEducation;

	@ApiModelProperty(value = "候选人职务")
	String candidatePost;

	@ApiModelProperty(value = "候选人职称")
	String candidatePsition;
	@ApiModelProperty(value = "候选人民族")
	String candidateNation;
	@ApiModelProperty(value = "候选人生日")
	String candidateBirthday;

	@ApiModelProperty(value = "候选人学校")
	String candidateSchool;

	@ApiModelProperty(value = "近两年是否担任专职辅导员")
	String candidateIsInstructor;

	@ApiModelProperty(value = "候选人是否在岗在编")
	String candidateRegularPayroll;

	@ApiModelProperty(value = "打分人性别")
	String raterSex;

	@ApiModelProperty(value = "打分人学历")
	String raterEducation;

	@ApiModelProperty(value = "打分人职务")
	String raterpost;
	@ApiModelProperty(value = "打分人职称")
	String raterpositio;
	@ApiModelProperty(value = "打分人学校")
	String raterSchool;


	@ApiModelProperty(value = "比赛id") 
	int scoreInfoId;
	
	@ApiModelProperty(value = "阶段id")
	@NotNull
	int stageId;
	
	@ApiModelProperty(value = "比赛Name")
	String scoringTitle;
	
	@ApiModelProperty(value = "阶段名称") 
	String stageName;
	
	@ApiModelProperty(value = "评分人id")
	int raterId;

	@ApiModelProperty(value="评分人姓名")
	String raterName;



	@ApiModelProperty(value = "选手id")
	@NotNull
	int candidateId;

	@ApiModelProperty(value = "选手姓名")
	String candidateName;
	
	@ApiModelProperty(value = "选手number")
	int candidateNum;
	
	
	@ApiModelProperty(value = "选手成绩")
	@NotNull
	@Min(0)
	@Max(100)
	int fraction = 0;
	 
	@ApiModelProperty(value = "打分时间")
	protected Timestamp createTime;
	
	@ApiModelProperty(value = "修改时间")
	protected Timestamp updateTime;
	
	@ApiModelProperty(value = "打分增加或修改")
	int verify;
	
	
//	public Grade( int stageId, int currId,int raterId, int score){
//		this.setCandidateId(currId);
//		this.setRaterId(raterId);
//		this.setStageId(stageId);
//		this.setScore(score);
//	}


	public void setScoreInfoId(int scoreInfoId) {
		this.scoreInfoId = scoreInfoId;
	}

	public void setStageId(int stageId) {
		this.stageId = stageId;
	}

	public void setScoringTitle(String scoringTitle) {
		this.scoringTitle = scoringTitle;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public void setRaterId(int raterId) {
		this.raterId = raterId;
	}

	public void setRaterName(String raterName) {
		this.raterName = raterName;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}

	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}

	public void setCandidateNum(int candidateNum) {
		this.candidateNum = candidateNum;
	}

	public void setFraction(int fraction) {
		this.fraction = fraction;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public void setVerify(int verify) {
		this.verify = verify;
	}

	public int getScoreInfoId() {
		return scoreInfoId;
	}

	public int getStageId() {
		return stageId;
	}

	public String getScoringTitle() {
		return scoringTitle;
	}

	public String getStageName() {
		return stageName;
	}

	public int getRaterId() {
		return raterId;
	}

	public String getRaterName() {
		return raterName;
	}

	public int getCandidateId() {
		return candidateId;
	}

	public String getCandidateName() {
		return candidateName;
	}

	public int getCandidateNum() {
		return candidateNum;
	}

	public int getFraction() {
		return fraction;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public int getVerify() {
		return verify;
	}

	public String getCandidateSex() {
		return candidateSex;
	}

	public String getCandidateEducation() {
		return candidateEducation;
	}

	public String getCandidatePost() {
		return candidatePost;
	}

	public String getCandidatePsition() {
		return candidatePsition;
	}

	public String getCandidateNation() {
		return candidateNation;
	}

	public String getCandidateBirthday() {
		return candidateBirthday;
	}

	public String getCandidateSchool() {
		return candidateSchool;
	}

	public String getCandidateIsInstructor() {
		return candidateIsInstructor;
	}

	public String getCandidateRegularPayroll() {
		return candidateRegularPayroll;
	}

	public String getRaterSex() {
		return raterSex;
	}

	public String getRaterEducation() {
		return raterEducation;
	}

	public String getRaterpost() {
		return raterpost;
	}

	public String getRaterpositio() {
		return raterpositio;
	}

	public String getRaterSchool() {
		return raterSchool;
	}

	public void setCandidateSex(String candidateSex) {
		this.candidateSex = candidateSex;
	}

	public void setCandidateEducation(String candidateEducation) {
		this.candidateEducation = candidateEducation;
	}

	public void setCandidatePost(String candidatePost) {
		this.candidatePost = candidatePost;
	}

	public void setCandidatePsition(String candidatePsition) {
		this.candidatePsition = candidatePsition;
	}

	public void setCandidateNation(String candidateNation) {
		this.candidateNation = candidateNation;
	}

	public void setCandidateBirthday(String candidateBirthday) {
		this.candidateBirthday = candidateBirthday;
	}

	public void setCandidateSchool(String candidateSchool) {
		this.candidateSchool = candidateSchool;
	}

	public void setCandidateIsInstructor(String candidateIsInstructor) {
		this.candidateIsInstructor = candidateIsInstructor;
	}

	public void setCandidateRegularPayroll(String candidateRegularPayroll) {
		this.candidateRegularPayroll = candidateRegularPayroll;
	}

	public void setRaterSex(String raterSex) {
		this.raterSex = raterSex;
	}

	public void setRaterEducation(String raterEducation) {
		this.raterEducation = raterEducation;
	}

	public void setRaterpost(String raterpost) {
		this.raterpost = raterpost;
	}

	public void setRaterpositio(String raterpositio) {
		this.raterpositio = raterpositio;
	}

	public void setRaterSchool(String raterSchool) {
		this.raterSchool = raterSchool;
	}

	@Override
	public String toString() {
		return "Grade{" +
				"scoreInfoId=" + scoreInfoId +
				", stageId=" + stageId +
				", scoringTitle='" + scoringTitle + '\'' +
				", stageName='" + stageName + '\'' +
				", raterId=" + raterId +
				", raterName='" + raterName + '\'' +
				", candidateId=" + candidateId +
				", candidateName='" + candidateName + '\'' +
				", candidateNum=" + candidateNum +
				", fraction=" + fraction +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				", verify=" + verify +
				'}';
	}
}
