package cn.swust.score.pojo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScoreInfo implements Serializable {

    @ApiModelProperty(value = "id")
    @Min(1)
	public Integer id;

    @Excel(name="打分标题",orderNum="1")
    @ApiModelProperty(value = "打分标题")
	@NotNull
	@Size(min = 1,max =20 )
    public String scoringTitle;

    @Excel(name="打分时间",orderNum="2")
    @ApiModelProperty(value = "打分时间")
	@NotNull
	public Date scoringTime;


    private static final long serialVersionUID = 1L;


	public Integer getId () {
		return id;
	}
		
	public void setId (Integer id) {
		this.id= id ;
	}
    public String getScoringTitle () {   
    	 return scoringTitle;
    }

    public void setScoringTitle (String scoringTitle) {
    	 this.scoringTitle= scoringTitle == null ? null : scoringTitle.trim();
    }


	public Date getScoringTime () {   
	    return scoringTime;
	}

	public void setScoringTime (Date scoringTime) {
	    this.scoringTime= scoringTime;
	}

	@Override
	public String toString() {
		return "ScoreInfo{" +
				"id=" + id +
				", scoringTitle='" + scoringTitle + '\'' +
				", scoringTime=" + scoringTime +
				'}';
	}
}