package cn.swust.score.pojo.dev;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;

public class GenerateInput implements Serializable {
 
 
	@ApiModelProperty(value="保存路径")
	private String path = "D:/generate";
	@ApiModelProperty(value="数据表名")
	private String tableName; 
	@ApiModelProperty(value="Pojo包名")
	private String beanPackageName;
	@ApiModelProperty(value="mybatisMapper包名")
	private String mybatisMapperPackageName;
	@ApiModelProperty(value="dao包名")
	private String daoPackageName;
	@ApiModelProperty(value="service包名")
	private String servicePackageName;	 
	@ApiModelProperty(value="controller包名")
	private String controllerPkgName;
	@ApiModelProperty(value="html包名")
	private String htmlPackageName;
	
	
	@ApiModelProperty(value="Pojo 类名 可以不填写")
	private String beanName;		 
	@ApiModelProperty(value="mybatisMapper类名")
	private String mybatisMapperName;
	@ApiModelProperty(value="dao类名")
	private String daoName;			
	@ApiModelProperty(value="service类名")
	private String serviceName;	
	@ApiModelProperty(value="controller类名")
	private String controllerName;
	@ApiModelProperty(value="html类名")
	private String htmlName;
	@ApiModelProperty(value="类映射url")
	private String classUrl;
	
	public String getClassUrl() {
		return classUrl;
	}

	public void setClassUrl(String classUrl) {
		this.classUrl = classUrl;
	}

	public String getHtmlPackageName() {
		return htmlPackageName;
	}

	public void setHtmlPackageName(String htmlPackageName) {
		this.htmlPackageName = htmlPackageName;
	}

	public String getHtmlName() {
		return htmlName;
	}

	public void setHtmlName(String htmlName) {
		this.htmlName = htmlName;
	}

	private List<BeanField> fields;
	
	 
	
	public String getMybatisMapperName() {
		return mybatisMapperName;
	}

	public void setMybatisMapperName(String mybatisMapperName) {
		this.mybatisMapperName = mybatisMapperName;
	}
	 
	public String getMybatisMapperPackageName() {
		return mybatisMapperPackageName;
	}

	public void setMybatisMapperPackageName(String mybatisMapperPackageName) {
		this.mybatisMapperPackageName = mybatisMapperPackageName;
	}
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getBeanPackageName() {
		return beanPackageName;
	}

	public void setBeanPackageName(String beanPackageName) {
		this.beanPackageName = beanPackageName;
	}

	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public String getDaoPackageName() {
		return daoPackageName;
	}

	public void setDaoPackageName(String daoPackageName) {
		this.daoPackageName = daoPackageName;
	}

	public String getDaoName() {
		return daoName;
	}

	public void setDaoName(String daoName) {
		this.daoName = daoName;
	}

	public String getServicePackageName() {
		return servicePackageName;
	}

	public void setServicePackageName(String servicePackageName) {
		this.servicePackageName = servicePackageName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getControllerPkgName() {
		return controllerPkgName;
	}

	public void setControllerPkgName(String controllerPkgName) {
		this.controllerPkgName = controllerPkgName;
	}

	public String getControllerName() {
		return controllerName;
	}

	public void setControllerName(String controllerName) {
		this.controllerName = controllerName;
	}

	public List<BeanField> getFields() {
		return fields;
	}

	public void setFields(List<BeanField> fields) {
		this.fields = fields;
	}

}
