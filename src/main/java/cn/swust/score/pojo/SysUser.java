package cn.swust.score.pojo;
 
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;  
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 
import io.swagger.annotations.ApiModelProperty; 
@JsonInclude(Include.NON_NULL)
public class SysUser  {
	private Long id;

	 	@NotNull 
	@Size(min=1, max=11 )
	@ApiModelProperty(value = "登陆用户")
	private String username; 
	@Size(min=1, max=60 )
	
	@JsonIgnore
	@NotNull 
	@ApiModelProperty(value = "登陆密码")
	private String password;
	
	@Size(min=1)
	@NotNull 
	@ApiModelProperty(value = "昵称")
	private String nickname;
	@Size(min=1)
	@ApiModelProperty(value = "头像")
	private String headImgUrl;
	
	 
	@Size(min=1, max=1 )
	@ApiModelProperty(value = "语言")
	private String lang;
	// @NotNull(message = "电话号码不能为空")
    
	@ApiModelProperty(value = "性别")
	@Pattern(regexp = "[FM]")
	@Size(min = 1,max = 1)
	private String sex;
	@NotNull
	@ApiModelProperty(value = "状态:不可用 0;可用 1;锁定2;")
	private Long status;
	 
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getHeadImgUrl() {
		return headImgUrl;
	}
	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}  
	
	public interface Status {
		int DISABLED = 0;
		int VALID = 1;
		int LOCKED = 2;
	}
}
