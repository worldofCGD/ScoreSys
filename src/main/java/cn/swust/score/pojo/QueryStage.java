package cn.swust.score.pojo;

import cn.swust.score.basepojo.BaseQueryParam;

import java.sql.Date;

public class QueryStage extends BaseQueryParam {


    public Integer scoreInfoId;

    private static final long serialVersionUID = 1L;


    public Integer getScoreInfoId () {
        return scoreInfoId;
    }
    public void setScoreInfoId (Integer scoreInfoId) {
        this.scoreInfoId= scoreInfoId ;
    }

}
