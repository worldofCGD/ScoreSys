package cn.swust.score.pojo;

import java.sql.Date;

import javax.validation.constraints.NotNull;

import cn.swust.score.basepojo.BaseQueryParam;

public class QueryCandidate extends BaseQueryParam{
	 
	    public String name;

		public String number;

		public String department;

		public String sex;

		public String education;

		public String post;

		public String position;

		public String briefIntroduction;

		public Double totalScore;

		public String nation;

		public Date birthdayStart;
		public Date birthdayEnd;

		public String school;

		public String isInstructor;

		public String regularPayroll;

		public String academicDegree;

		public String politicalOutlook;

		public String officePhone;

		public String phone;

		public String email;

		public Integer sort;
		
		@NotNull
		public Integer scoreInfoId;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getNumber() {
			return number;
		}

		public void setNumber(String number) {
			this.number = number;
		}

		public String getDepartment() {
			return department;
		}

		public void setDepartment(String department) {
			this.department = department;
		}

		public String getSex() {
			return sex;
		}

		public void setSex(String sex) {
			this.sex = sex;
		}

		public String getEducation() {
			return education;
		}

		public void setEducation(String education) {
			this.education = education;
		}

		public String getPost() {
			return post;
		}

		public void setPost(String post) {
			this.post = post;
		}

		public String getPosition() {
			return position;
		}

		public void setPosition(String position) {
			this.position = position;
		}

		public String getBriefIntroduction() {
			return briefIntroduction;
		}

		public void setBriefIntroduction(String briefIntroduction) {
			this.briefIntroduction = briefIntroduction;
		}

		public Double getTotalScore() {
			return totalScore;
		}

		public void setTotalScore(Double totalScore) {
			this.totalScore = totalScore;
		}

		public String getNation() {
			return nation;
		}

		public void setNation(String nation) {
			this.nation = nation;
		}

		public Date getBirthdayStart() {
			return birthdayStart;
		}

		public void setBirthdayStart(Date birthdayStart) {
			this.birthdayStart = birthdayStart;
		}

		public Date getBirthdayEnd() {
			return birthdayEnd;
		}

		public void setBirthdayEnd(Date birthdayEnd) {
			this.birthdayEnd = birthdayEnd;
		}

		public String getSchool() {
			return school;
		}

		public void setSchool(String school) {
			this.school = school;
		}

		public String getIsInstructor() {
			return isInstructor;
		}

		public void setIsInstructor(String isInstructor) {
			this.isInstructor = isInstructor;
		}

		public String getRegularPayroll() {
			return regularPayroll;
		}

		public void setRegularPayroll(String regularPayroll) {
			this.regularPayroll = regularPayroll;
		}

		public String getAcademicDegree() {
			return academicDegree;
		}

		public void setAcademicDegree(String academicDegree) {
			this.academicDegree = academicDegree;
		}

		public String getPoliticalOutlook() {
			return politicalOutlook;
		}

		public void setPoliticalOutlook(String politicalOutlook) {
			this.politicalOutlook = politicalOutlook;
		}

		public String getOfficePhone() {
			return officePhone;
		}

		public void setOfficePhone(String officePhone) {
			this.officePhone = officePhone;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public Integer getSort() {
			return sort;
		}

		public void setSort(Integer sort) {
			this.sort = sort;
		}

		public Integer getScoreInfoId() {
			return scoreInfoId;
		}

		public void setScoreInfoId(Integer scoreInfoId) {
			this.scoreInfoId = scoreInfoId;
		}
		

}
