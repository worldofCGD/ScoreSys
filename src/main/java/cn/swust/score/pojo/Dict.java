package cn.swust.score.pojo;

public class Dict  {

	private Integer id;
	private String type;
	private String k;
	private String val;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getK() {
		return k;
	}
	public void setK(String k) {
		this.k = k;
	}
	public String getVal() {
		return val;
	}
	public void setVal(String val) {
		this.val = val;
	} 
}
