package cn.swust.score.pojo;
 
import java.sql.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.swust.score.basepojo.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
 
public class QueryRater extends BaseQueryParam {
	
	@Excel(name="评分人姓名",orderNum="1")
	@ApiModelProperty(value = "评分人姓名") 

	@Size(min=1,max=50)
	public String name;
	
	@Excel(name="评分人工号",orderNum="2")
	@ApiModelProperty(value = "评分人工号") 
	@Size(min=1,max=50)
	public String number;
	
	@Excel(name="评分人部门",orderNum="3")
	@ApiModelProperty(value = "评分人部门") 
	@Size(min=1,max=50)
	public String department;
	
	@Excel(name="评分人性别",orderNum="4")
	@ApiModelProperty(value = "评分人性别") 
	public String sex;
	
	@Excel(name="评分人学历",orderNum="5")
	@ApiModelProperty(value = "评分人学历") 
	@Size(min=1,max=50)
	public String education;
	
	@Excel(name="评分人职务",orderNum="6")
	@ApiModelProperty(value = "评分人职务") 
	@Size(min=1,max=50)
	public String post;
	
	@Excel(name="评分人职称",orderNum="7")
	@ApiModelProperty(value = "评分人职称") 
	@Size(min=1,max=50)
	public String positio;
	
	@Excel(name="登陆密码",orderNum="8")
	@ApiModelProperty(value = "登陆密码") 
	@Size(min=1,max=50)
	public String password;
	
	@Excel(name="评分人学校",orderNum="9")
	@ApiModelProperty(value = "评分人学校") 
	@Size(min=1,max=50)
	public String school;
	
	@Excel(name="查询创建表的时间",databaseFormat="yyMMddHHmmss",format="yyyy-MM-dd",orderNum="11")
	@ApiModelProperty(value = "查询创建表的时间")
	public Date creatTimeStart;
	
	@Excel(name="查询结束表的时间",databaseFormat="yyMMddHHmmss",format="yyyy-MM-dd",orderNum="12")
	@ApiModelProperty(value = "查询结束表的时间")
	public Date creatTimeEnd;

	@Excel(name="评分ID",orderNum="10")
	@ApiModelProperty(value = "评分ID")
	@Min(1)
	public Integer scoreInfoId;
	
    public Date getCreatTimeStart() {
		return creatTimeStart;
	}

	public void setCreatTimeStart(Date creatTimeStart) {
		this.creatTimeStart = creatTimeStart;
	}

	public Date getCreatTimeEnd() {
		return creatTimeEnd;
	}

	public void setCreatTimeEnd(Date creatTimeEnd) {
		this.creatTimeEnd = creatTimeEnd;
	}

	public Integer getscoreInfoId() {
		return scoreInfoId;
	}

	public void setscoreInfoId(Integer scoreInfoId) {
		this.scoreInfoId = scoreInfoId;
	}

	public String getName () {   
    	 return name;
    }

    public void setName (String name) {
    	 this.name= name == null ? null : name.trim();
    }

    public String getNumber () {   
    	 return number;
    }

    public void setNumber (String number) {
    	 this.number= number == null ? null : number.trim();
    }

    public String getDepartment () {   
    	 return department;
    }

    public void setDepartment (String department) {
    	 this.department= department == null ? null : department.trim();
    }

    public String getSex () {   
    	 return sex;
    }

    public void setSex (String sex) {
    	 this.sex= sex == null ? null : sex.trim();
    }

    public String getEducation () {   
    	 return education;
    }

    public void setEducation (String education) {
    	 this.education= education == null ? null : education.trim();
    }

    public String getPost () {   
    	 return post;
    }

    public void setPost (String post) {
    	 this.post= post == null ? null : post.trim();
    }

    public String getPositio () {   
    	 return positio;
    }

    public void setPositio (String positio) {
    	 this.positio= positio == null ? null : positio.trim();
    }

    public String getPassword () {   
    	 return password;
    }

    public void setPassword (String password) {
    	 this.password= password == null ? null : password.trim();
    }

    public String getSchool () {   
    	 return school;
    }

    public void setSchool (String school) {
    	 this.school= school == null ? null : school.trim();
    }

}
