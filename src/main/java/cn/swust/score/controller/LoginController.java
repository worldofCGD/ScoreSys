package cn.swust.score.controller;

import java.util.ArrayList;
import java.util.List; 
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.swust.score.basepojo.ResultResponse;
import cn.swust.score.pojo.Permission;
import cn.swust.score.pojo.SysUser;
import io.swagger.annotations.ApiOperation;
 
@RestController
public class LoginController {
	 
	@PostMapping("/rater/login")
	public  ResultResponse<String>  login(String username, String password) { 
		ResultResponse<String> result = new ResultResponse(0, "OK", "mytesttoken"); 
		return result;
	}
	
	@GetMapping("/rater/logout")
	public  ResultResponse<String>  login() { 
		ResultResponse<String> result = new ResultResponse(0, "OK", "mytesttoken"); 
		return result;
	}
	
	
	@PostMapping("/admin/login")
	public  ResultResponse<String>  adminLogin(String username, String password) { 
		ResultResponse<String> result = new ResultResponse(0, "OK", "mytesttoken"); 
		return result;
	}
	
	@GetMapping("/admin/logout")
	public  ResultResponse<String>  adminLogin() { 
		ResultResponse<String> result = new ResultResponse(0, "OK", "mytesttoken"); 
		return result;
	}
	
	@GetMapping("/rater/current")
	@ApiOperation(value = "当前登录用户") 
	public ResultResponse<SysUser> currentUser() {
		SysUser user = new SysUser();
		user.setUsername("测试用户");
		return  new ResultResponse<SysUser> (0, "OK", user); 
	}
	
	@GetMapping("/notices/count-unread")
	public ResultResponse<Integer> getCount() { 
		ResultResponse<Integer> result = new ResultResponse(0, "OK", 5); 
		return result; 
 } 

	@GetMapping("/permissions/current")
	public ResultResponse<List<Permission>> permissionsCurrent() { 
		List<Permission> plist = new ArrayList<Permission>();
		Permission menu1 = new Permission();
		menu1.setId(0L);
		menu1.setName("菜单1");
		Permission menu2 = new Permission();
		menu2.setId(1L);
		menu2.setName("菜单2");
		
		List<Permission> pchildlist = new ArrayList<Permission>();
		Permission menu3 = new Permission();
		menu3.setId(3L);
		menu3.setName("字典");
		menu3.setHref("pages/dict/dictList.html");
		Permission menu4 = new Permission();
		menu4.setId(4L);
		menu4.setName("候选人");
		menu4.setHref("pages/candidate/candidateList.html");
		pchildlist.add(menu3);
		pchildlist.add(menu4);
		menu1.setChild(pchildlist);
		plist.add(menu1);
		plist.add(menu2);
		return new  ResultResponse<List<Permission>>(plist);
 }
}
