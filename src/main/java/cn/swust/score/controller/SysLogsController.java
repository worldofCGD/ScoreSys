package cn.swust.score.controller;
  
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping; 
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.swust.score.basepojo.ResultResponse;
import cn.swust.score.pojo.QuerySysLogs;
import cn.swust.score.pojo.SysLogs;
import cn.swust.score.service.SysLogsService;
import cn.swust.score.service.impl.SysLogsServiceImpl;
import io.swagger.annotations.Api; 
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "日志")
@RestController
@RequestMapping("/api/logs")
public class SysLogsController {
	@Autowired
	SysLogsService logService;
 	 
	
	@ApiOperation(value = "根据ID查询记录")  
	@PutMapping(value = "/get") 
	public ResultResponse<SysLogs> getSysLogsById(
			@ApiParam(required = true, value = "查询记录编号")
			@RequestBody @Valid Integer id ) {
		SysLogs SysLogs = logService.getById(id);
		return new ResultResponse<SysLogs>(SysLogs);
	}

	@ApiOperation(value = "根据ID删除记录")  
	@DeleteMapping(value = "/del") 
	public ResultResponse<Integer> delSysLogsById(
			@ApiParam(required = true, value = "查询记录编号")
			@RequestBody @Valid Integer id ) {
		int count = logService.delById(id);
		return new ResultResponse<Integer>(count);
	} 
	
	 
	
	@ApiOperation(value = "详细列表查询") 
	@PostMapping(value = "/list")
	public ResultResponse<PageInfo<SysLogs>> listSysLogs(
			 
			@ApiParam(required = false, value = "查询参数")
			@Valid @RequestBody QuerySysLogs param) {
		return new ResultResponse<PageInfo<SysLogs>>(
				logService.list(param.getPageNum(), param.getPageSize(),  param));
	}

	  
	
	@ApiOperation(value = "删除选选年份，几月到几月的日志")  
	@DeleteMapping(value = "/delmonth") 
	public ResultResponse<Integer> delMonthSysLogs
	(@ApiParam(required = true, value = "年")
			@Valid @Min(2018) @RequestBody int year,
			@ApiParam(required = true, value = "开始的月份")
			@Min(1) @Max(12) @RequestBody int start, 
			@ApiParam(required = true, value = "开始的月份")
			@Min(1) @Max(12) @RequestBody int end) {
		int count = logService.delMonthLogs(year, start, end);
		return new ResultResponse<Integer>(count);
	} 
	
	@ApiOperation(value="导出excel")
	@ApiParam(required = true, value="查询参数")
	@PostMapping(value="/excel")
	public void listexcel(
			@ApiParam(required = false,value = "查询参数")
			@Valid @RequestBody QuerySysLogs param,HttpServletResponse response) throws IOException {
		PageInfo<SysLogs> listData = 
				logService.list(param.getPageNum(), param.getPageSize(), param);
	
		Workbook workbook = 
				ExcelExportUtil.exportExcel(new ExportParams(), SysLogs.class,listData.getList());
				
		// 告诉浏览器用什么软件可以打开此文件
	    response.setHeader("content-Type", "application/vnd.ms-excel");
	    // 下载文件的默认名称
	    response.setHeader("Content-Disposition", "attachment;filename=iftSystemCode.xls");
	   	workbook.write(response.getOutputStream());
	} 
	 
}
