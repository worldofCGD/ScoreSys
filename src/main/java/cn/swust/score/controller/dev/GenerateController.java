package cn.swust.score.controller.dev; 

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.apiclub.captcha.filter.image.RotateFilter;
import cn.swust.score.pojo.dev.GenerateInput;
import cn.swust.score.service.GenerateService;
import cn.swust.score.util.Freemarker;
import io.swagger.annotations.Api; 
import io.swagger.annotations.ApiOperation;

/**
 * 代码生成接口
 *  
 */
@Api(tags = "代码生成")
@RestController
@RequestMapping("/generate")
public class GenerateController {

	@Autowired
	private GenerateService generateService;

	  
	 
	@ApiOperation("生成代码")
	@PostMapping(value="/createCode")
	public GenerateInput save(@RequestBody GenerateInput input) throws Exception {
		String tableName = generateService.upperFirstChar(input.getTableName());
		if (input.getBeanName()==null)		
			input.setBeanName(generateService.upperFirstChar(tableName));
		
		if (input.getDaoName()==null)		
			input.setDaoName(generateService.upperFirstChar(tableName)+"Dao");
	
		if (input.getMybatisMapperName()==null)		
			input.setMybatisMapperName(generateService.upperFirstChar(tableName)+"Mapper");

		if (input.getServiceName()==null)		
			input.setServiceName(generateService.upperFirstChar(tableName)+"Service");
	
		if (input.getControllerName()==null)		
			input.setControllerName(generateService.upperFirstChar(tableName)+"Controller");
		
		if (input.getHtmlName()==null)		
			input.setHtmlName(generateService.upperFirstChar(tableName)+"Html");

		input.setFields(generateService.listBeanField(input.getTableName()));
	
		
		String beanPackageName = input.getBeanPackageName();
		String beanName = input.getBeanName();
		String htmlPackageName = input.getHtmlPackageName();
		String daoPackageName = input.getDaoPackageName();
		String mybatisMapperPackageName = input.getMybatisMapperPackageName();
		String servicePackageName = input.getServicePackageName();
		String controllerPackageName = input.getControllerPkgName();
		String classUrl = input.getClassUrl();
		Map<String,Object> root = new HashMap<String,Object>();		//创建数据模型
		root.put("fieldList", input.getFields());
		root.put("beanPackageName", beanPackageName);						//首字母大写
		root.put("beanName", beanName);								//包名首字母大写	
		root.put("nowDate", new Date());							//当前日期
		root.put("daoPackageName", daoPackageName);
		root.put("servicePackageName",servicePackageName);
		root.put("controllerPackageName",controllerPackageName);
		root.put("tableName", input.getTableName());
		root.put("classUrl", classUrl);
		
		System.out.println("我的url路径是:"+classUrl);
		
		String filePath = "admin/ftl/code/";		
		//存放路径
		String ftlPath =  "createCode/";								//ftl路径
		/*生成pojo*/
		Freemarker.printFile("pojoTemplate.ftl", root, beanPackageName+"/"+beanName+".java", filePath, ftlPath);

		/*生成dao*/
		Freemarker.printFile("daoTemplate.ftl", root, daoPackageName+"/"+beanName+"Dao.java", filePath, ftlPath);

		/*生成service*/
		Freemarker.printFile("serviceTemplate.ftl", root, servicePackageName+"/"+beanName+"Service.java", filePath, ftlPath);

		/*生成serviceImp*/
		Freemarker.printFile("serviceImplTemplate.ftl", root, servicePackageName+"/Impl/"+beanName+"ServiceImpl.java", filePath, ftlPath);

		/*生成controller*/
		Freemarker.printFile("controllerTemplate.ftl", root, controllerPackageName+"/"+beanName+"Controller.java", filePath, ftlPath);

		/*生成mapper*/
		Freemarker.printFile("mybatisMapperTemplate.ftl", root, mybatisMapperPackageName+"/"+beanName+"Mapper.xml", filePath, ftlPath);

		/*生成addHtmlT*/
		Freemarker.printFile("addHtmlTemplate.ftl", root, htmlPackageName+"/"+classUrl+"Add.html", filePath, ftlPath);

		/*生成listHtml*/
		Freemarker.printFile("listHtmlTemplate.ftl", root, htmlPackageName+"/"+classUrl+"List.html", filePath, ftlPath);

		/*生成updateHtml*/
		Freemarker.printFile("updateHtmlTemplate.ftl", root, htmlPackageName+"/"+classUrl+"Update.html", filePath, ftlPath);
		return input;
	}
	
	

}
