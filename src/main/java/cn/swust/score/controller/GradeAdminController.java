package cn.swust.score.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.swust.score.basepojo.ResultResponse; 
import cn.swust.score.pojo.CandidateGrade;

import cn.swust.score.pojo.ShowGrade;
import cn.swust.score.pojo.StageGrade;
import cn.swust.score.service.GradeAdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "管理员评分")
@RestController
@Validated
@RequestMapping("/admin/grade")
public class GradeAdminController extends BaseController{
	@Autowired
	public GradeAdminService gradeadminservice;
	@ApiOperation(value = "获取阶段打分信息") 
	@PostMapping("/get") 
	public ResultResponse<List<ShowGrade>> getCandidat (
			@ApiParam(required = true, value = "评分信息")
			@Valid @RequestBody Integer stageId, Integer refreshid) {
		if (refreshid > this.getRefreshId())
			return new  ResultResponse<List<ShowGrade>>(gradeadminservice.getCandidate(stageId));
		else
			return new ResultResponse<List<ShowGrade>>(gradeadminservice.getCandidate(stageId));
	}
	
	
	@ApiOperation(value = "获取某个阶段打分信息") 
	@PostMapping("/get/stage") 
	public ResultResponse<List<StageGrade>> getStageGrade(
			@ApiParam(required = true, value = "评分信息")
			@Valid @RequestBody Integer stageId) {		 
			return new ResultResponse<List<StageGrade>>(gradeadminservice.getStageGrade(stageId));
	}
	
	@ApiOperation(value = "获取某个阶段打分信息")
	@PostMapping("/stage/excel")
	public void stageExcel(@ApiParam(required = true, value = "评分信息") @Valid @RequestBody Integer stageId,
			HttpServletResponse response) throws IOException {

		List<StageGrade> list = gradeadminservice.getStageGrade(stageId);

		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), StageGrade.class, list);

		// 告诉浏览器用什么软件可以打开此文件
		response.setHeader("content-Type", "application/vnd.ms-excel");
		// 下载文件的默认名称
		response.setHeader("Content-Disposition", "attachment;filename=阶段信息.xls");
		workbook.write(response.getOutputStream());
}

	
	
	@ApiOperation(value = "获取所有阶段打分信息") 
	@PostMapping("/getgrade") 
	public ResultResponse<List<CandidateGrade>> getAllGrade(
			@ApiParam(required = true, value = "评分信息")
			@Valid @RequestBody Integer scoreInfoId) {		 
			return new ResultResponse<List<CandidateGrade>>(gradeadminservice.getAllGrade(scoreInfoId));
	}
	
	
	@ApiOperation(value = "获取所有阶段打分信息") 
	@PostMapping("/grade/excel") 
	public void excel(
			@ApiParam(required = true, value = "评分信息")
			@Valid @RequestBody Integer scoreInfoId,HttpServletResponse response) throws IOException{
		List<CandidateGrade> list = gradeadminservice.getAllGrade(scoreInfoId);
		
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), CandidateGrade.class, list);
		// 告诉浏览器用什么软件可以打开此文件
		response.setHeader("content-Type", "application/vnd.ms-excel");
		// 下载文件的默认名称
		response.setHeader("Content-Disposition", "attachment;filename=阶段信息.xls");
		workbook.write(response.getOutputStream());
	}
	
	
}
