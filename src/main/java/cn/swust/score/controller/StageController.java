package cn.swust.score.controller;

import cn.swust.score.basepojo.ResultResponse;
import cn.swust.score.pojo.Stage;
import cn.swust.score.service.StageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "分数信息")
@RestController
@Validated
@RequestMapping("/stage")
public class StageController {

	@Autowired
	StageService stageService;
	
	@ApiOperation(value="删除记录")
	@DeleteMapping(value="/del")
	public ResultResponse<Integer> delById(
	@ApiParam(required=true,value="查询记录编号") @RequestBody Integer id) {
		return new ResultResponse<Integer>( stageService.delById(id));
	}
	

	@ApiOperation(value="新增记录")
	@PostMapping(value="/add")
	@ApiParam(required = true, value = "记录值")
	public ResultResponse<Stage> save(@Valid @RequestBody Stage record) {
	    if(stageService.save(record)<=0)
	        record=null;
	    else
	        record=stageService.getById(record.getId());
		return new ResultResponse<Stage>(record);
	}
	

	@ApiOperation(value = "根据ID查询记录") 
	@PutMapping(value = "/get")
	public ResultResponse<Stage> getById(
			@ApiParam(required = true, value = "查询记录编号")
			@RequestBody @Valid Integer id) {
		return new ResultResponse<Stage>(stageService.getById(id));
	}

	@ApiParam(required = true , value = "记录内容")
	@PutMapping(value = "/edit") 
	public ResultResponse<Stage> editById(@Valid @RequestBody Stage record) {
		if(stageService.edit(record)<=0)
	        record=null;
	    else
	        record=stageService.getById(record.getId());
		return new ResultResponse<Stage>(record);
	}

	@ApiOperation(value = "详细列表查询")
	@PostMapping("/list")
	public ResultResponse<List<Stage>> list
			(@ApiParam(required = false, value = "查询参数")
			@Valid @RequestBody Integer scoreInfoId) {
		List<Stage> stageList=stageService.list(scoreInfoId);	//阶段分数信息查询，只需阶段信息id;
		if(stageList.isEmpty())
			return new ResultResponse<List<Stage>>("return data is empty!");
		else
			return new ResultResponse<List<Stage>>(stageList);
	}

}
