package cn.swust.score.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import cn.swust.score.pojo.StageGrade;
import cn.swust.score.service.GradeService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.swust.score.basepojo.ResultResponse;
import cn.swust.score.pojo.Candidate;
import cn.swust.score.pojo.Dict;
import cn.swust.score.pojo.Grade;
import cn.swust.score.service.CandidateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import  cn.swust.score.service.CandidateStagInfoService;
import java.util.List;

@Api(tags = "评分人评分")
@RestController
@Validated
@RequestMapping("/grade")
public class GradeController extends BaseController {
	
	@Autowired
	public CandidateService candidateService;
	
	@ApiOperation(value = "获取下一个评分人") 
	@PostMapping("/next") 
	public ResultResponse<Candidate> getNextCandidate(
			@ApiParam(required = true, value = "比赛id")
			@Valid @RequestBody Integer scoreInfoId,
			@ApiParam(required = true, value = "当前评分人序号id，第一个为0")
			Integer sort) {
		return new ResultResponse<Candidate>(candidateService.getNext(scoreInfoId, sort));
	}
	 
	@ApiOperation(value = "打分，增加和修改")
	@PostMapping("/set")
	public ResultResponse<Grade> set (
			@ApiParam(required = true, value = "评分信息")
			@Valid @RequestBody Grade grad) {
		int re = candidateService.setScore(grad.getCandidateId(),
				this.getRateId(),grad.getStageId(),grad.getFraction());
		int code = 0;
		String msg="";
		switch (re){
			case 1: //"新增成功";
				code = 0;
				msg = "新增成功";
				break;
			case  2://"新增失败";
				code = 1;
				msg = "新增失败";
				break;
			case 3://"修改成功";
				code = 0;
				msg = "修改成功";
				break;
			case 4://"修改失败";
				code = 1;
				msg = "修改失败";
				break;
			case 5://"成绩已经确认不能修改";
				code = 1;
				msg = "成绩已经确认不能修改";
				break;
			case 6://"评分人和候选人学校相同，拒绝评分"
				code = 1;
				msg = "评分人和候选人学校相同，拒绝评分";
				break;
		}
		return new ResultResponse<Grade>(code,msg,grad);
	}
	
	 
	@ApiOperation(value = "获取本人打分信息")
	@PostMapping("/getall")
	public ResultResponse<List<Grade>>getAllGrade(
			@ApiParam(required = true, value = "打分信息")
			@Valid @RequestBody  Integer stageId) {
		return new ResultResponse<List<Grade>>(candidateService.listByRaterId( this.getRateId(), stageId));
	}


	@Autowired
	GradeService gradeService;

	@ApiOperation(value = "确认提交")
	@PostMapping("/commit")
	public ResultResponse<List<StageGrade> > commite(@ApiParam(required = true, value = "评分阶段")
	@Valid @RequestBody Integer stageId) {
		//设置 确认
		//计算得分，排名 candidate_stag_info
		int raterId=this.getRateId();
		List<StageGrade> candidateStagInfos=gradeService.commitService(raterId,stageId);
		return new ResultResponse<List<StageGrade>>(0,"OK",candidateStagInfos);
	} 
}
