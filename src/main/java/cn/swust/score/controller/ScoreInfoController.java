package cn.swust.score.controller;

import java.util.List;

import cn.swust.score.basepojo.ResultResponse;
import cn.swust.score.pojo.QueryScoreInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.swust.score.pojo.ScoreInfo;
import cn.swust.score.service.ScoreInfoService;

import javax.validation.Valid;

@Api(tags = "打分阶段标题和时间信息")
@RestController
@Validated
@RequestMapping("/scoreInfo")
public class ScoreInfoController {

	@Autowired
	public  ScoreInfoService scoreInfoService;
	
	@ApiOperation(value="删除记录")
	@DeleteMapping(value="/del")
	public ResultResponse<Integer> delById(
	@ApiParam(required=true,value="查询记录编号") @RequestBody Integer id) {
		return new ResultResponse<Integer>( scoreInfoService.delById(id));
	}
	

	@ApiOperation(value="新增记录")
	@PostMapping(value="/add")
	@ApiParam(required = true, value = "记录值")
	public ResultResponse<ScoreInfo> save(@Valid @RequestBody ScoreInfo record) {
	    if(scoreInfoService.save(record)<=0)
	        record=null;
	    else
	        record=scoreInfoService.getById(record.getId());
		return new ResultResponse<ScoreInfo>(record);
	}
	

	@ApiOperation(value = "根据ID查询记录") 
	@PutMapping(value = "/get")
	public ResultResponse<ScoreInfo> getById(
			@ApiParam(required = true, value = "查询记录编号")
			@RequestBody @Valid Integer id) {
		return new ResultResponse<ScoreInfo>(scoreInfoService.getById(id));
	}

	@ApiParam(required = true , value = "记录内容")
	@PutMapping(value = "/edit") 
	public ResultResponse<ScoreInfo> editById(@Valid @RequestBody ScoreInfo record) {
		if(scoreInfoService.edit(record)<=0)
	        record=null;
	    else
	        record=scoreInfoService.getById(record.getId());
		return new ResultResponse<ScoreInfo>(record);
	}

	@ApiOperation(value = "详细列表查询") 
	@PostMapping("/list")
	public ResultResponse<List<ScoreInfo>> list
			(@ApiParam(required = false, value = "查询参数")
			@Valid @RequestBody QueryScoreInfo param) {
		List<ScoreInfo> scoreInfoList=scoreInfoService.list(param);
		if(scoreInfoList.isEmpty())
		 return new ResultResponse<List<ScoreInfo>>("return data is empty!");
		else
			return new ResultResponse<List<ScoreInfo>>(scoreInfoList);
	}

}
