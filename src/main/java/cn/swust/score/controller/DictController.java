package cn.swust.score.controller;
  
import javax.validation.Valid; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated; 
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController; 
import com.github.pagehelper.PageInfo;

import cn.swust.score.basepojo.ResultResponse;
import cn.swust.score.pojo.Dict;
import cn.swust.score.pojo.QueryDict; 
import cn.swust.score.service.DictService;
import io.swagger.annotations.Api; 
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "字典")
@RestController
@Validated
@RequestMapping("/dicts")
public class DictController{ 
	@Autowired
	private DictService dictService;  
	
	@ApiOperation(value = "新增记录")
	@ApiParam(required = true, value = "记录值")
	@PostMapping("/add") 
	public ResultResponse<Dict> saveDict(@Valid @RequestBody  Dict record) {
		if( dictService.save(record) <= 0) 
			record = null; 
		else
			record = dictService.getById(record.getId());
		return new ResultResponse<Dict>(record);
	}

	@ApiOperation(value = "修改记录")
	@ApiParam(required = true, value = "记录内容")
	@PutMapping("/edit") 
	public ResultResponse<Dict> editDict(@Valid @RequestBody Dict record) {
		if( dictService.edit(record) <= 0)
			record = null; 
		else
			record = dictService.getById(record.getId());
		return new ResultResponse<Dict>(record); 
	}
	 
	@ApiOperation(value = "根据ID查询记录") 
	@PostMapping("/get") 
	public  ResultResponse<Dict> getDictById(
			@ApiParam(required = true, value = "查询记录编号")
			@RequestBody  Integer id) { 
		return new ResultResponse<Dict>(dictService.getById(id));
		 
	}

	@ApiOperation(value = "根据ID删除记录") 
	@PostMapping("/del") 
	public ResultResponse<Integer> delDictById(
			@ApiParam(required = true, value = "查询记录编号")
			@RequestBody Integer id) {
		return new ResultResponse<Integer>(dictService.delById(id));
	}  

	@ApiOperation(value = "详细列表查询") 
	@PostMapping("/list")
	public ResultResponse<PageInfo<Dict>> listDict 
			(@ApiParam(required = false, value = "查询参数")
			@Valid @RequestBody QueryDict param) {
		 return new  ResultResponse<PageInfo<Dict>>(dictService.list(param.getPageNum(),param.getPageSize()));
	} 
} 