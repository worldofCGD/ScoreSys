package cn.swust.score.dao;
 


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.swust.score.pojo.StageGrade;

import java.util.List;

@Mapper
public interface CandidateStagInfoDao {
   StageGrade getGradeByCandidateId(@Param("candidateId")int candidateId, @Param("stageId") int stageId);
   int delById(Integer id);

   int save(StageGrade record);

   StageGrade getById(Integer id);

   int editById(StageGrade record);

   //获取此阶段candidate;
   List<StageGrade> getByStageId(int stageId);

   List<StageGrade> list();
}
