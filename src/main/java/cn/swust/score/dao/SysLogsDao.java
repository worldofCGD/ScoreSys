package cn.swust.score.dao;
 
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.swust.score.pojo.QuerySysLogs;
import cn.swust.score.pojo.SysLogs; 


@Mapper
public interface SysLogsDao  {
	/**
 	* 删除记录
 	* @param id 主键id
 	* @return 删除记录数量
 	*/
	int delById(Integer id);	 
 
    /**
     * 插入记录
     * @param record 基于基本信息的记录
     * @return 插入记录数量
     */
    int save(SysLogs record);
 
    /**
     * 根据主键查询记录
     * @param id 主键id
     * @return 查询出的记录
     */
    SysLogs getById(Integer id);
      
  
    /**
     *  根据ID更新更新
     * @param record 记录 
     * @return 更新记录数量
     */
    int editById(SysLogs record); 
    
    /**
     * 分页查询记录
     * @param page 查询条件及分页信息
     * @return 记录
     */
    List<SysLogs> list(QuerySysLogs param); 
    
  
	@Delete("delete from sys_logs where YEAR(create_time) = #{y} and Month(create_time) >= #{s} and Month(create_time) <= #{e}")
	int deleteLogs(@Param("y") int year, @Param("s") int start, @Param("e") int end);
}
