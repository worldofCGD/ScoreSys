package cn.swust.score.dao;

import java.util.List;

import cn.swust.score.pojo.QueryScoreInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import cn.swust.score.pojo.ScoreInfo;

@Mapper
public interface ScoreInfoDao {
    int delById(Integer id);

    int save(ScoreInfo record);

    ScoreInfo getById(Integer id);

    int editById(ScoreInfo record);
    
    @Select("select scoring_title from score_info where id=#{id}")
    char getTitle(Integer id);
    
    List<ScoreInfo> list(QueryScoreInfo param);
}