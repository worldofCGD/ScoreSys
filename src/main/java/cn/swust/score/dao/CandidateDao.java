package cn.swust.score.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.swust.score.pojo.Candidate;
import cn.swust.score.pojo.QueryCandidate;

@Mapper
public interface CandidateDao {
    int delById(Integer id);

    int save(Candidate record);

    Candidate getById(Integer id);

    int editById(Candidate record);
    
    List<Candidate> list(QueryCandidate param);  
    
	Candidate getNext(@Param("scoreInfoId") Integer scoreInfoId, @Param("sort") Integer sort);
	
	@Select("select average from candidate_stag_info where candidate_id=#{candidateId} and stage_id=#{stageId}")
	int getStageScore(@Param("candidateId")int candidateId,@Param("stageId") int stageId);


	
}