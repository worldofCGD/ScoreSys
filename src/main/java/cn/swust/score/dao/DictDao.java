package cn.swust.score.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.swust.score.pojo.Dict;
 

@Mapper
public interface DictDao {

	@Select("select * from sys_dict t where t.id = #{id}")
	Dict getById(Integer id);

	@Delete("delete from sys_dict where id = #{id}")
	int delete(Integer id);

	int update(Dict dict);

	@Options(useGeneratedKeys = true, keyProperty = "id")
	@Insert("insert into sys_dict(type, k, val, createTime, updateTime) values(#{type}, #{k}, #{val}, now(), now())")
	int save(Dict dict);

	int count(@Param("params") Map<String, Object> params);

	List<Dict> list();

 
}
