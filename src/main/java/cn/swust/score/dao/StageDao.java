package cn.swust.score.dao;

import java.util.List;

import cn.swust.score.pojo.QueryStage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import cn.swust.score.pojo.Stage;

@Mapper
public interface StageDao {
    int delById(Integer id);

    int save(Stage record);

    Stage getById(Integer id);

    int editById(Stage record);
    
    List<Stage> list(Integer scoreInfoId);
    
    @Select("select score_info_id from stage where id = #{id}")
    int getScoreIdById(Integer id);
    
   

}