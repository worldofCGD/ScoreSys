package cn.swust.score.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import cn.swust.score.pojo.Grade;

 

@Mapper
public interface GradeDao { 
	@Insert("insert into candidate_score(candidate_id," + "rater_id,stage_id,fraction) value ("
			+ "#{currId}, #{raterId}, #{stageId}, #{score})")
	int setScore(@Param("stageId") int stageId, @Param("currId") int currId, @Param("raterId") int raterId,
			@Param("score") int score);

	@Update("update candidate_score set fraction = #{score} where rater_id=#{raterId} and stage_id= #{stageId}")
	int updateScore(@Param("stageId") int stageId, @Param("candidateId")int candidateId, @Param("raterId") int raterId,
			@Param("score") int score);
	
	
	@Select("select * from candidate_score where rater_id=#{raterId} and stage_id = #{stageId} and candidate_id = #{candidateID}")
	Grade get( @Param("raterId") int raterId, @Param("stageId") int stageId, @Param("currId") int currId);

	@Select("select average from candidate_stag_info where candidate_id=#{candidateId} and stage_id=#{stageId}")
	int getStageScore(@Param("candidateId")int candidateId,@Param("stageId") int stageId);

	
	List<Grade> listByRaterId(@Param("raterId")int raterId,@Param("stageId")int stageId);

	List<Grade> listByCandidateId(@Param("candidateId")int candidateId,@Param("stageId")int stageId);


	//将verify置为1;
	@Update("update candidate_score set verify=1 where stage_id= #{stageId} and rater_id=#{raterId} and candidate_id=#{candidateId}")
	int updataSetVerify(@Param("raterId") int raterId,@Param("stageId") int stageId, @Param("candidateId")int candidateId );

	List<Grade> getAll(int stageId);
	


}
