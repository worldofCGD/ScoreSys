package cn.swust.score.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.repository.query.Param;

import cn.swust.score.pojo.QueryRater;
import cn.swust.score.pojo.Rater;

@Mapper
public interface RaterDao {
    int delById(Integer id);
   
    int save(Rater record);

    Rater getById(Integer id);

    int editById(Rater record);
    
    @Select("select * from rater by score_info_id = #{id}")
    List<Rater> getByScoreId(Integer id);
    
    List<Rater> list(QueryRater param);
}