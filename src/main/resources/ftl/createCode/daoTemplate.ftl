package ${daoPackageName};

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import ${beanPackageName}.${beanName};

@Mapper
public interface ${beanName}Dao {
    int delById(Integer id);

    int save(${beanName} record);

    ${beanName} getById(Integer id);

    int editById(${beanName} record);
    
    List<${beanName}> list(); 
}