package ${servicePackageName};

import ${beanPackageName}.${beanName};
import java.util.List;
import com.github.pagehelper.PageInfo;

public interface ${beanName}Service {

	int delById(Integer id);

    int save(${beanName} record);

    ${beanName} getById(Integer id);

    int edit(${beanName} record);
    
    PageInfo<${beanName}> list(int pagenum, int pagesize); 
}