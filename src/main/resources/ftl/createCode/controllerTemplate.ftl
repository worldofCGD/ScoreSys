package ${controllerPackageName};

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ${beanPackageName}.${beanName};
import ${servicePackageName}.${beanName}Service;

@RestController
public class ${beanName}Controller {

	@Autowired
	public  ${beanName?uncap_first}Service;
	
	@ApiOperation(value="删除记录")
	@DeleteMapping(value="/del")
	public ResultResponse<Integer> delById(
	@ApiParam(required=true,value="查询记录编号") @RequestBody Integer id) {
		return new ResultResponse<Integer>( ${beanName?uncap_first}Service.delById(id));
	}
	

	@ApiOperation(value="新增记录")
	@PostMapping(value="/add")
	@ApiParam(required = true, value = "记录值")
	public ResultResponse<${beanName}> save(@Valid  @RequestBody ${beanName} record) {
	    if(${beanName?uncap_first}Service.save(record)<=0)
	        record=null;
	    else
	        record=${beanName?uncap_first}Service.getById(record.getId());
		return new ResultResponse<${beanName}>(record);
	}
	

	@ApiOperation(value = "根据ID查询记录") 
	@PutMapping(value = "/get")
	public ResultResponse<${beanName}> getById(
			@ApiParam(required = true, value = "查询记录编号")
			@RequestBody @Valid Integer id) {
		return new ResultResponse<${beanName}>(${beanName?uncap_first}.getById(id));
	}

	@ApiParam(required = true , value = "记录内容")
	@PutMapping(value = "/edit") 
	public ResultResponse<${beanName}> editById(@Valid @RequestBody ${beanName} record) {
		if(${beanName?uncap_first}Service.edit(record)<=0)
	        record=null;
	    else
	        record=${beanName?uncap_first}Service.getById(record.getId());
		return new ResultResponse<${beanName}>(record);
	}

	@ApiOperation(value = "详细列表查询") 
	@PostMapping("/list")
	public ResultResponse<PageInfo<${beanName}>> list
			(@ApiParam(required = false, value = "查询参数")
			@Valid @RequestBody Query${beanName} param) {
		 return new  ResultResponse<PageInfo<${beanName}>>(${beanName?uncap_first}Service.list(param.getPageNum(),param.getPageSize()));
	}

}
