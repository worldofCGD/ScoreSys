package ${servicePackageName}.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ${daoPackageName}.${beanName}Dao;
import ${beanPackageName}.${beanName};
import ${servicePackageName}.${beanName}Service;
import com.github.pagehelper.PageInfo;

@Service
public class ${beanName}ServiceImpl implements ${beanName}Service{

	@Autowired
	public ${beanName}Dao ${beanName?uncap_first}dao;
	
	@Override
	public int delById(Integer id) {
		return ${beanName?uncap_first}dao.delById(id);
	}

	@Override
	public int save(${beanName} record) {
		return ${beanName?uncap_first}dao.save(record);
	}

	@Override
	public ${beanName} getById(Integer id) {
		return ${beanName?uncap_first}dao.getById(id);
	}


	@Override
	public int edit(${beanName} record) {
		return ${beanName?uncap_first}dao.editById(record);
	}

	@Override
	public PageInfo<${beanName}> list(int pagenum, int pagesize) {
		PageHelper.startPage(pagenum, pagesize);
		List<${beanName}> datalist = ${beanName?uncap_first}dao.list();
			 
		PageInfo<${beanName}> p = new PageInfo<${beanName}>(datalist);
		return p;
	}

}
