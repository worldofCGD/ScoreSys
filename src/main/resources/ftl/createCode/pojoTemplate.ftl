package ${beanPackageName};

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;


public class ${beanName} implements Serializable {
	<#list fieldList as var>
	public <#if var.type=='String'>String</#if><#if var.type=='Long'>Long</#if><#if var.type=='Integer'>Integer</#if><#if var.type=='Double'>Double</#if><#if var.type=='Float'>Float</#if><#if var.type=='BigDecimal'>BigDecimal</#if><#if var.type=='Date'>Date</#if> ${var.name};

	</#list>

    private static final long serialVersionUID = 1L;

    <#list fieldList as var>
       <#if var.type=='String'>
    public String get${var.name?cap_first} () {   
    	 return ${var.name};
    }

    public void set${var.name?cap_first} (String ${var.name}) {
    	 this.${var.name}= ${var.name} == null ? null : ${var.name}.trim();
    }
       </#if>

       <#if var.type=='Integer'>
	public Integer get${var.name?cap_first} () {   
		  return ${var.name};
	}
	
	public void set${var.name?cap_first} (Integer ${var.name}) {
		  this.${var.name}= ${var.name} ;
	}
	</#if>
	<#if var.type=='Long'>
	public Long get${var.name?cap_first} () {   
		return ${var.name};
	}
		
	public void set${var.name?cap_first} (Long ${var.name}) {
		this.${var.name}= ${var.name} ;
	}
	</#if>
	<#if var.type=='Double'>
	public Double get${var.name?cap_first} () {   
		  return ${var.name};
	}
	
	public void set${var.name?cap_first} (Double ${var.name}) {
		  this.${var.name}= ${var.name};
	}
    </#if>
    <#if var.type=='Float'>
	public Float get${var.name?cap_first} () {   
		return ${var.name};
	}
		
	public void set${var.name?cap_first} (Float ${var.name}) {
		this.${var.name}= ${var.name};
	}
   	</#if>
   	<#if var.type=='BigDecimal'>
	public BigDecimal get${var.name?cap_first} () {   
	    return ${var.name};
	}

	public void set${var.name?cap_first} (BigDecimal ${var.name}) {
	    this.${var.name}= ${var.name};
	}
	</#if>
	<#if var.type=='Date'>
	public Date get${var.name?cap_first} () {   
	    return ${var.name};
	}

	public void set${var.name?cap_first} (Date ${var.name}) {
	    this.${var.name}= ${var.name};
	}
	</#if>
	</#list> 
}