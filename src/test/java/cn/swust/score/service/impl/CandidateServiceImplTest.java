package cn.swust.score.service.impl;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 
import cn.swust.score.pojo.Candidate;
import cn.swust.score.service.CandidateService;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class CandidateServiceImplTest {
	@Autowired
	CandidateService service;
	@Test
	public void testDelById() {
		Integer id = 5;
		service.delById(5);
		Candidate c = service.getById(5); 
		Assert.assertTrue(c == null); 
	}

	@Test
	public void testSetRanking()
	{
		service.setRanking();
	}
}
