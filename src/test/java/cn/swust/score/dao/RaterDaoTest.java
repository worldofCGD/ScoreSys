package cn.swust.score.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import cn.swust.score.pojo.QueryRater;
import cn.swust.score.pojo.Rater;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class RaterDaoTest {
	@Autowired
	RaterDao testObject; 
	@Test
	public void testSelectByPrimaryKey() {
		 
/*	    Rater t = testObject.getById(1);
		System.out.println(t.toString());*/
		
		//更新数据的时候 名字，ID，地址，学校是必须填的，才能完成更新
/*		Rater record = new Rater();
		record.setName("王五");
		record.setNumber("12345");
		record.setId(4);
		record.setSchool("西南科技大学");
		record.setDepartment("绵阳");
		record.setPassword("345");
		int t = testObject.editById(record);
		System.out.println(t);*/
		
/*		int t = testObject.delById(4);
		System.out.println(t);*/
		
/*		QueryRater param = new QueryRater();
		param.setName("王");
		//param.setPassword("232");
		List<Rater> r = testObject.list(param);
		for (Rater rater : r) {
			System.out.println(rater);
		}
		*/
		//更新数据的时候 名字，ID，地址，创建时间，学校是必须填的 
		Rater record = new Rater();
		record.setName("王五");
		record.setNumber("12345");
		record.setDepartment("绵阳");
		record.setSchool("西南科技大学");
		record.setPassword("3456");
		Date createTime = new Date(2018, 5, 1);
		record.setCreateTime(createTime);
		int t = testObject.save(record);
		System.out.println(t);
		
	}
}
