package cn.swust.score.dao;

import cn.swust.score.pojo.QueryScoreInfo;
import cn.swust.score.pojo.ScoreInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ScoreInfoDaoTest {

    @Autowired
    ScoreInfoDao scoreInfoDao;

    @Test
    public void listTest(){
        QueryScoreInfo param=new QueryScoreInfo();
        param.setId((long)1);
        List<ScoreInfo> ScoreInfolist=scoreInfoDao.list(param);
        for(ScoreInfo s:ScoreInfolist)
            System.out.println(s.toString());
    }

}
