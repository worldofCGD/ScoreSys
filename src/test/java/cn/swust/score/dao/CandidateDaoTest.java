package cn.swust.score.dao;

import static org.junit.Assert.*;

import cn.swust.score.service.CandidateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 

import cn.swust.score.pojo.Candidate;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class CandidateDaoTest {
	@Autowired
	CandidateDao testObject;

	@Autowired
	CandidateService candidateService;
	@Test
	public void testSelectByPrimaryKey() {
		Candidate t = testObject.getById(1);
		System.out.println(t.toString());
	}

	@Test
	public void getNextTest(){

		System.out.println(candidateService.getNext(1,6).toString());
	}
}
