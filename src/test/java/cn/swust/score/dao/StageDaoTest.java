package cn.swust.score.dao;

import cn.swust.score.pojo.Stage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class StageDaoTest {
    @Autowired
    StageDao stageDao;
    @Test
    public void getByIdTest(){
      System.out.println(stageDao.getById(5));
    }
    @Test
    public void stageListTest(){
        List<Stage> stageList=stageDao.list(1);
        for(Stage s:stageList)
            System.out.println(s.toString());
    }
    @Test
    public void saveTest(){
        Stage stage=new Stage();
        stage.name="33";
        stage.startTime=now();
        stage.endTime=now();
        stage.highestScore=4.0;
        stage.lowestScore=4.0;
        stage.average=4.0;
        stage.scoreInfoId=1;
        stage.fullMarks="3";

        System.out.println(stageDao.save(stage));
        System.out.println(new Date(System.currentTimeMillis()));
    }


    public static Date now(){
        return new Date(System.currentTimeMillis());
    }

}
