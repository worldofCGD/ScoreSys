package cn.swust.score.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.alibaba.fastjson.JSONObject; 

import cn.swust.score.pojo.Candidate;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class CandidateControllerTest {
	@Autowired
	CandidateController testObject; 
	private MockMvc mockMvc;

	@Before 
	public void setUp() throws Exception {
		 MockitoAnnotations.initMocks(this);  
		this.mockMvc = MockMvcBuilders.standaloneSetup(testObject).build();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSave() throws Exception {
		Candidate c = new Candidate();
		c.setId(1);
		c.setSex("F");
		c.setDepartment("342");
		c.setName("abvc"); 
		c.setScoreInfoId(1);
		String jsonStr = JSONObject.toJSONString(c);
		 
		    MvcResult result = mockMvc.perform(post("/candidate/add")  
		            .contentType(MediaType.APPLICATION_JSON).content(jsonStr)  
		            .accept(MediaType.APPLICATION_JSON)) //执行请求   
		            .andExpect(status().isOk())
	                .andExpect(jsonPath("code").exists())
	             	.andDo(MockMvcResultHandlers.print())
		            .andReturn();  
	}  
}
