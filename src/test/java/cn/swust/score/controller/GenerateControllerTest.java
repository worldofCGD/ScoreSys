package cn.swust.score.controller;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.io.FileInputStream;
import java.io.OutputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.alibaba.fastjson.JSONObject;

import cn.swust.score.controller.dev.GenerateController;
import cn.swust.score.pojo.dev.GenerateInput;
import io.swagger.annotations.ApiModelProperty;
 
@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class GenerateControllerTest {

  
		@Autowired
		GenerateController testObject;
		private MockMvc mockMvc;
		@Autowired  
		private WebApplicationContext wac;  
		@Before
		public void setUp() throws Exception { 
			//MockMvcBuilders使用构建MockMvc对象   （项目拦截器有效） 
			mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();  
			//单个类  拦截器无效
			//this.mockMvc = MockMvcBuilders.standaloneSetup(testObject).build();
		}

		@After
		public void tearDown() throws Exception {
		}

		@Test
		public void testSave() throws Exception {
			GenerateInput t = new GenerateInput();
			t.setTableName("score_info");
			t.setBeanPackageName("cn.swust.score.pojo");
			t.setDaoPackageName("cn.swust.score.dao");
			t.setServicePackageName("cn.swust.score.service");	 
			t.setControllerPkgName("cn.swust.score.controller");	 
			String jsonStr = JSONObject.toJSONString(t); 
			MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/generate/createCode");
			builder.contentType(MediaType.APPLICATION_JSON); 
			builder.content(jsonStr);
			MvcResult result = mockMvc.perform(builder).andReturn();
			
			  
			byte[] outstr = result.getResponse().getContentAsByteArray();
			String srt2=new String(outstr,"UTF-8");
			System.out.println(srt2);

		} 
	}
