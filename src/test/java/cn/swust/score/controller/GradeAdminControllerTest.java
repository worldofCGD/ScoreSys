package cn.swust.score.controller;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.MediaType;
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class GradeAdminControllerTest {
	private  MockMvc mockMvc;
	@Autowired  
	private WebApplicationContext wac;
	
	@Before
	public  void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

//	@Before
//	public void setUp() throws Exception {
//	}
//
//	@After
//	public void tearDown() throws Exception {
//	}

	@Test
	public void testGetAllGrade() throws Exception {
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post
				("/admin/grade/getgrade");
		Integer scoreInfoId = 1;
		builder.contentType(MediaType.APPLICATION_JSON);
		String jsonstr = JSONObject.toJSONString(scoreInfoId);
		builder.content(jsonstr);
		MvcResult result = mockMvc.perform(builder).andReturn();
		System.out.println(result);
	}

}
